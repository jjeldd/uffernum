package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_67_67_allEffect extends ActorScript
{          	
	
public var _tempNum:Float;

public var _tempMvspd:Float;
    
/* ========================= Custom Event ========================= */
public function _customEvent_effBlindDuring():Void
{

}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effBurningSec():Void
{
        actor.say("entityDefaultInitilization", "_customBlock_taketruedamage", [5]);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effCurseCreate():Void
{
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusDef"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusDef", (_tempNum - 5));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effCurseEnd():Void
{
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusDef"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusDef", (_tempNum + 5));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effHasteCreate():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd + 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effHasteEnd():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd - 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effMagicShieldCreate():Void
{
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusDef"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusDef", (_tempNum + 5));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effMagicShieldEnd():Void
{
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusDef"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusDef", (_tempNum - 5));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effPotionDuring():Void
{
        sayToScene("sceneInit", "_customBlock_actorHeal", [actor,((actor.getValue("entityDefaultInitilization", "_healthMax") * 0.10) * (1 / 60))]);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effRegenSec():Void
{
        actor.say("entityDefaultInitilization", "_customBlock_healHealth", [5]);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSilencedDuring():Void
{
        actor.setValue("entityDefaultInitilization", "_canCast", false);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSilencedEnd():Void
{
        actor.setValue("entityDefaultInitilization", "_canCast", true);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSlowedCreate():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd - 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd + 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSpringyCreate():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percJumpForce"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percJumpForce", (_tempMvspd + 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSpringyEnd():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percJumpForce"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percJumpForce", (_tempMvspd - 0.2));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effStunnedEnd():Void
{

}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSwiftArrowsCreate():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd + 0.5));
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusAttackSpeed"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusAttackSpeed", (_tempNum + 1));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_effSwiftArrowsEnd():Void
{
        _tempMvspd = asNumber(actor.getValue("entityDefaultInitilization", "_percMvSpeed"));
propertyChanged("_tempMvspd", _tempMvspd);
        actor.setValue("entityDefaultInitilization", "_percMvSpeed", (_tempMvspd - 0.5));
        _tempNum = asNumber(actor.getValue("entityDefaultInitilization", "_bonusAttackSpeed"));
propertyChanged("_tempNum", _tempNum);
        actor.setValue("entityDefaultInitilization", "_bonusAttackSpeed", (_tempNum - 1));
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("tempNum", "_tempNum");
_tempNum = 0.0;
nameMap.set("tempMvspd", "_tempMvspd");
_tempMvspd = 0.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		
	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
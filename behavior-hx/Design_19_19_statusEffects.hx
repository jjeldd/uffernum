package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_19_19_statusEffects extends ActorScript
{          	
	
public var _actorToRead:Actor;

public var _maxEffectsToShow:Float;

public var _effectFromPlayer:Float;

public var _gapAftereachSkill:Float;

public var _effDrawList:Array<Dynamic>;

public var _effListFromPlayer:Array<Dynamic>;

public var _tempImageInst:Bitmap;

public var _tempImage:BitmapData;

public var _effImageList:Array<Dynamic>;

public var _effImageInstList:Array<Dynamic>;

public var _effSecondsList:Array<Dynamic>;

public var _effMask:BitmapData;

public var _effmaxSecondsList:Array<Dynamic>;

public var _cooldownTemptext:String;

public var _effUpdated:Array<Dynamic>;

public var _timerImageInstList:Array<Dynamic>;

public var _timerImageList:Array<Dynamic>;

public var _tempSecondsImageInst:Bitmap;

public var _tempSeconds:BitmapData;

public var _textInMargin:Float;

public var _timerUpdate:Array<Dynamic>;

public var _tempColor:Int;

public var _tempImageTop:BitmapData;
    
/* ========================= Custom Block ========================= */


/* Params are: __slot */
public function _customBlock_updateSlot(__slot:Float):Void
{
var __Self:Actor = actor;
        _effectFromPlayer = asNumber(List2D.get_entry(Std.int(0), Std.int(__slot), _effListFromPlayer));
propertyChanged("_effectFromPlayer", _effectFromPlayer);
        if(!(_effectFromPlayer == -1))
{
            _effDrawList[Std.int(__slot)] = _effectFromPlayer;
            /* "Get the image from the database of the spell ID" */
            _tempImage = getExternalImage(List2D.get_entry(Std.int(6), Std.int(_effectFromPlayer), getGameAttribute("maineffectsList")));
propertyChanged("_tempImage", _tempImage);
            /* "Get the top layer of the image" */
            _tempImageTop = getExternalImage("StatusEffectFront.png");
propertyChanged("_tempImageTop", _tempImageTop);

            drawImageOnImage(_tempImageTop, _tempImage, Std.int(0), Std.int(0), BlendMode.NORMAL);
            _effImageList[Std.int(__slot)] = _tempImage;
            _tempImageInst = new Bitmap(_effImageList[Std.int(__slot)]);
propertyChanged("_tempImageInst", _tempImageInst);
            _effImageInstList[Std.int(__slot)] = _tempImageInst;
            _effSecondsList[Std.int(__slot)] = List2D.get_entry(Std.int(1), Std.int(__slot), _effListFromPlayer);
            _effmaxSecondsList[Std.int(__slot)] = List2D.get_entry(Std.int(2), Std.int(__slot), _effListFromPlayer);
}

}
    
/* ========================= Custom Block ========================= */


/* Params are: __Slot */
public function _customBlock_updTimer(__Slot:Float):Void
{
var __Self:Actor = actor;
        _tempSeconds = new BitmapData(Std.int(32 * Engine.SCALE), Std.int(32 * Engine.SCALE), true, 0);
propertyChanged("_tempSeconds", _tempSeconds);
        drawTextOnImage(_tempSeconds, ("" + Math.round(asNumber(List2D.get_entry(Std.int(1), Std.int(__Slot), _effListFromPlayer)))), Std.int(0), Std.int(0), getFont(2));
        _timerImageList[Std.int(__Slot)] = _tempSeconds;
        _tempSecondsImageInst = new Bitmap(_timerImageList[Std.int(__Slot)]);
propertyChanged("_tempSecondsImageInst", _tempSecondsImageInst);
        _timerImageInstList[Std.int(__Slot)] = _tempSecondsImageInst;
}

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("actorToRead", "_actorToRead");
nameMap.set("maxEffectsToShow", "_maxEffectsToShow");
_maxEffectsToShow = 5.0;
nameMap.set("effectFromPlayer", "_effectFromPlayer");
_effectFromPlayer = 0.0;
nameMap.set("gapAftereachSkill", "_gapAftereachSkill");
_gapAftereachSkill = 40.0;
nameMap.set("effDrawList", "_effDrawList");
_effDrawList = [];
nameMap.set("effListFromPlayer", "_effListFromPlayer");
_effListFromPlayer = [];
nameMap.set("tempImageInst", "_tempImageInst");
nameMap.set("tempImage", "_tempImage");
nameMap.set("effImageList", "_effImageList");
_effImageList = [];
nameMap.set("effImageInstList", "_effImageInstList");
_effImageInstList = [];
nameMap.set("Actor", "actor");
nameMap.set("effSecondsList", "_effSecondsList");
_effSecondsList = [];
nameMap.set("effMask", "_effMask");
nameMap.set("effmaxSecondsList", "_effmaxSecondsList");
_effmaxSecondsList = [];
nameMap.set("cooldownTemptext", "_cooldownTemptext");
_cooldownTemptext = "";
nameMap.set("effUpdated", "_effUpdated");
_effUpdated = [];
nameMap.set("timerImageInstList", "_timerImageInstList");
_timerImageInstList = [];
nameMap.set("timerImageList", "_timerImageList");
_timerImageList = [];
nameMap.set("tempSecondsImageInst", "_tempSecondsImageInst");
nameMap.set("tempSeconds", "_tempSeconds");
nameMap.set("textInMargin", "_textInMargin");
_textInMargin = 8.0;
nameMap.set("timerUpdate", "_timerUpdate");
_timerUpdate = [];
nameMap.set("tempColor", "_tempColor");
_tempColor = Utils.getColorRGB(0,0,0);
nameMap.set("tempImageTop", "_tempImageTop");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.disableActorDrawing();
        _effDrawList = new Array<Dynamic>();
propertyChanged("_effDrawList", _effDrawList);
        _effImageList = new Array<Dynamic>();
propertyChanged("_effImageList", _effImageList);
        _effImageInstList = new Array<Dynamic>();
propertyChanged("_effImageInstList", _effImageInstList);
        _effSecondsList = new Array<Dynamic>();
propertyChanged("_effSecondsList", _effSecondsList);
        _effmaxSecondsList = new Array<Dynamic>();
propertyChanged("_effmaxSecondsList", _effmaxSecondsList);
        _effUpdated = new Array<Dynamic>();
propertyChanged("_effUpdated", _effUpdated);
        _timerImageList = new Array<Dynamic>();
propertyChanged("_timerImageList", _timerImageList);
        _timerImageInstList = new Array<Dynamic>();
propertyChanged("_timerImageInstList", _timerImageInstList);
        _timerUpdate = new Array<Dynamic>();
propertyChanged("_timerUpdate", _timerUpdate);
        for(index0 in 0...Std.int(5))
{
            _effUpdated.push(false);
            _timerUpdate.push(true);
}

        /* "Image, ImageInst,Seconds,MaxSeconds,EffID" */
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        _effListFromPlayer = _actorToRead.getValue("entityDefaultInitilization", "_effectsList");
propertyChanged("_effListFromPlayer", _effListFromPlayer);
        for(index0 in 0...Std.int(_maxEffectsToShow))
{
            _effectFromPlayer = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), _effListFromPlayer));
propertyChanged("_effectFromPlayer", _effectFromPlayer);
            if((!(_effectFromPlayer == -1) && (_effUpdated[Std.int(index0)] == false)))
{
                actor.say("statusEffects", "_customBlock_updateSlot", [index0]);
                attachImageToActor(_effImageInstList[Std.int(index0)], actor, Std.int((index0 * _gapAftereachSkill)), Std.int(0), 2);

                _effUpdated[Std.int(index0)] = true;


}

            else if(((_effectFromPlayer == -1) && (_effUpdated[Std.int(index0)] == true)))
{

                removeImage(_effImageInstList[Std.int(index0)]);
                clearImagePartially(_effImageList[Std.int(index0)], Std.int((index0 * _gapAftereachSkill)), Std.int(0), Std.int(32), Std.int(32));
                _effUpdated[Std.int(index0)] = false;
}

}

}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        for(index0 in 0...Std.int(_maxEffectsToShow))
{
            _effListFromPlayer = _actorToRead.getValue("entityDefaultInitilization", "_effectsList");
propertyChanged("_effListFromPlayer", _effListFromPlayer);
            _effectFromPlayer = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), _effListFromPlayer));
propertyChanged("_effectFromPlayer", _effectFromPlayer);
            if(!(_effectFromPlayer == -1))
{
                if((asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _effListFromPlayer)) > 0))
{
                    g.alpha = ((100 * (asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _effListFromPlayer)) / asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _effListFromPlayer))))/100);
                    g.fillColor = Utils.getColorRGB(255,204,51);
                    g.fillRect((-3 + (index0 * _gapAftereachSkill)), (-3 + 38), 38, -((38 * (asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _effListFromPlayer)) / asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _effListFromPlayer))))));

}

}

}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
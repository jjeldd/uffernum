package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_62_62_skillAll extends ActorScript
{          	
	
public var _tempNum:Float;

public var _DistanceX:Float;

public var _DistanceY:Float;

public var _Facing:Float;
    
/* ========================= Custom Event ========================= */
public function _customEvent_skillEndSwiftArrows():Void
{
        sayToScene("sceneInit", "_customBlock_actorApplyEffect", [actor,11,5]);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_skillEndCycloneBlast():Void
{

}

    
/* ========================= Custom Event ========================= */
public function _customEvent_skillEndSplintedArrows():Void
{

        for(index0 in 0...Std.int(3))
{
            createRecycledActor(getActorType(130), actor.getX(), actor.getYCenter(), Script.BACK);
            sayToScene("sceneInit", "_customBlock_setProjVariables", [getLastCreatedActor(),((5 * actor.getValue("entityDefaultInitilization", "_level")) + (actor.getValue("entityDefaultInitilization", "_AGI") * 1)),0,actor,100]);
            getLastCreatedActor().setAngle(Utils.RAD * ((((cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float) - 20) + (20 * index0)) - 180)));
            getLastCreatedActor().applyImpulseInDirection(((cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float) - 20) + (20 * index0)), 100);
}

}

    
/* ========================= Custom Event ========================= */
public function _customEvent_skillEndStunningArrow():Void
{
        createRecycledActor(getActorType(174), actor.getX(), actor.getYCenter(), Script.BACK);
        sayToScene("sceneInit", "_customBlock_setProjVariables", [getLastCreatedActor(),((3.5 * actor.getValue("entityDefaultInitilization", "_level")) + (actor.getValue("entityDefaultInitilization", "_AGI") * 1.5)),0,actor,50]);
        getLastCreatedActor().setAngle(Utils.RAD * ((cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float) - 180)));
        getLastCreatedActor().applyImpulseInDirection(cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float), 120);
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_skillEndGoddessesPoison():Void
{

}

    
/* ========================= Custom Event ========================= */
public function _customEvent_novariaRightClickArrow():Void
{


        createRecycledActor(getActorType(130), actor.getX(), actor.getYCenter(), Script.BACK);
        sayToScene("sceneInit", "_customBlock_setProjVariables", [getLastCreatedActor(),(actor.getValue("entityDefaultInitilization", "_AGI") * (1 + (actor.getValue("entityDefaultInitilization", "_castingTime") / 100))),0,actor,100]);
        getLastCreatedActor().setAngle(Utils.RAD * ((cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float) - 180)));
        getLastCreatedActor().applyImpulseInDirection(cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float), (75 + actor.getValue("entityDefaultInitilization", "_castingTime")));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_novariaLeftClickMelee():Void
{
        createRecycledActor(getActorType(172), actor.getX(), actor.getY(), Script.BACK);
        sayToScene("sceneInit", "_customBlock_setProjVariables", [getLastCreatedActor(),(actor.getValue("entityDefaultInitilization", "_STR") * (1 + (actor.getValue("entityDefaultInitilization", "_castingTime") / 100))),0,actor,100]);
        getLastCreatedActor().applyImpulseInDirection(cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float), (75 + actor.getValue("entityDefaultInitilization", "_castingTime")));
        getLastCreatedActor().setAngle(Utils.RAD * ((cast((sayToScene("sceneInit", "_customBlock_retXYdegree", [actor.getXCenter(),actor.getYCenter(),getMouseX(),getMouseY()])), Float) - 180)));
}

    
/* ========================= Custom Event ========================= */
public function _customEvent_enemyChargeEnd():Void
{
        _DistanceX = asNumber((actor.getValue("dummyAi", "_targetActor").getXCenter() - actor.getXCenter()));
propertyChanged("_DistanceX", _DistanceX);
        _DistanceY = asNumber((actor.getValue("dummyAi", "_targetActor").getYCenter() - actor.getYCenter()));
propertyChanged("_DistanceY", _DistanceY);
        actor.applyImpulseInDirection(Utils.DEG * (Math.atan2(_DistanceY, _DistanceX)), 250);
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("tempNum", "_tempNum");
_tempNum = 0.0;
nameMap.set("Distance X", "_DistanceX");
_DistanceX = 0.0;
nameMap.set("Distance Y", "_DistanceY");
_DistanceY = 0.0;
nameMap.set("Facing", "_Facing");
_Facing = -90.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		
	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
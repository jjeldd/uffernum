package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class ActorEvents_110 extends ActorScript
{          	
	
public var _projDamage:Float;

public var _canBeKilled:Bool;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("projDamage", "_projDamage");
_projDamage = 15.0;
nameMap.set("canBeKilled", "_canBeKilled");
_canBeKilled = false;

	}
	
	override public function init()
	{
		    
/* ======================== Something Else ======================== */
addCollisionListener(actor, function(event:Collision, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((event.otherActor.getGroup() == getActorGroup(0)))
{
            sayToScene("sceneInit", "_customBlock_actorTakeDamage", [event.otherActor,_projDamage]);
            event.otherActor.applyImpulseInDirection(((Utils.DEG * (actor.getAngle()) - 180) + randomInt(Math.floor(-35), Math.floor(35))), 200);
            recycleActor(actor);
}

        if((_canBeKilled && (event.otherActor.getGroup() == getActorGroup(1))))
{
            createRecycledActor(getActorType(112), (actor.getX() + 8), actor.getY(), Script.BACK);
            getLastCreatedActor().setAngle(Utils.RAD * ((Utils.DEG * (actor.getAngle()) + randomInt(Math.floor(-25), Math.floor(25)))));
            recycleActor(actor);
}

        if((_canBeKilled && (event.otherActor.getGroup() == getActorGroup(6))))
{
            recycleActor(actor);
}

}
});
    
/* ======================= After N seconds ======================== */
runLater(1000 * 0.1, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        _canBeKilled = true;
propertyChanged("_canBeKilled", _canBeKilled);
}
}, actor);

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_48_48_arrowShooterBehaviour extends ActorScript
{          	
	
public var _rotationTime:Float;

public var _bulletsPerRotation:Float;

public var _semiRotTime:Float;

public var _angle:Float;

public var _FORCE:Float;
    
/* ========================= Custom Event ========================= */
public function _customEvent_shootBullet():Void
{
        if((getActorType(104) == actor.getType()))
{
            createRecycledActor(getActorType(110), (actor.getX() - 64), (actor.getY() + (actor.getHeight()/2)), Script.BACK);
}

        else if((getActorType(108) == actor.getType()))
{
            createRecycledActor(getActorType(110), (actor.getX() + 10), (actor.getY() + (actor.getHeight()/2)), Script.BACK);
}

        getLastCreatedActor().applyImpulseInDirection(_angle, _FORCE);
        getLastCreatedActor().setAngle(Utils.RAD * ((_angle - 180)));
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("rotationTime", "_rotationTime");
_rotationTime = 1.0;
nameMap.set("bulletsPerRotation", "_bulletsPerRotation");
_bulletsPerRotation = 1.0;
nameMap.set("semiRotTime", "_semiRotTime");
_semiRotTime = 0.5;
nameMap.set("angle", "_angle");
_angle = 180.0;
nameMap.set("FORCE", "_FORCE");
_FORCE = 50.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================= Every N seconds ======================== */
runPeriodically(1000 * _rotationTime, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        actor.say("arrowShooterBehaviour", "_customEvent_" + "shootBullet");
}
}, actor);

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
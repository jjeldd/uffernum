package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_71_71_projInitDefault extends ActorScript
{          	
	
public var _Damage:Float;

public var _TrueDamage:Float;

public var _Owner:Actor;

public var _OnHitApply:Array<Dynamic>;

public var _OnCastApply:Array<Dynamic>;

public var _CreationScript:String;

public var _StepScript:String;

public var _DeathScript:String;

public var _KnockbackForce:Float;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("Damage", "_Damage");
_Damage = 0.0;
nameMap.set("True Damage", "_TrueDamage");
_TrueDamage = 0.0;
nameMap.set("Owner", "_Owner");
nameMap.set("On Hit Apply", "_OnHitApply");
_OnHitApply = [];
nameMap.set("On Cast Apply", "_OnCastApply");
_OnCastApply = [];
nameMap.set("Creation Script", "_CreationScript");
_CreationScript = "";
nameMap.set("Step Script", "_StepScript");
_StepScript = "";
nameMap.set("Death Script", "_DeathScript");
_DeathScript = "";
nameMap.set("Knockback Force", "_KnockbackForce");
_KnockbackForce = 0.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        _OnCastApply = List2D.new_2D(Std.int(5), Std.int(2));
propertyChanged("_OnCastApply", _OnCastApply);
        _OnHitApply = List2D.new_2D(Std.int(5), Std.int(2));
propertyChanged("_OnHitApply", _OnHitApply);
        if((hasValue(_CreationScript) != false))
{
            actor.shout("_customEvent_" + _CreationScript);
}

    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((hasValue(_StepScript) != false))
{
            actor.shout("_customEvent_" + _StepScript);
}

}
});
    
/* ======================== Specific Actor ======================== */
addWhenKilledListener(actor, function(list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((hasValue(_DeathScript) != false))
{
            actor.shout("_customEvent_" + _DeathScript);
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
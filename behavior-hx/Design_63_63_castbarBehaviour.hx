package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_63_63_castbarBehaviour extends ActorScript
{          	
	
public var _actorToRead:Actor;

public var _opacity:Float;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("actorToRead", "_actorToRead");
nameMap.set("opacity", "_opacity");
_opacity = 0.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        _opacity = asNumber(_actorToRead.getValue("entityDefaultInitilization", "_castingBarAlpha"));
propertyChanged("_opacity", _opacity);
}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        g.alpha = ((_opacity * 100)/100);
        g.fillColor = Utils.getColorRGB(255,200,0);
        if(((_actorToRead.getValue("entityDefaultInitilization", "_castingTimeMax") > 0) && (_actorToRead.getValue("entityDefaultInitilization", "_castingTime") > 0)))
{
            g.fillRect(0, 0, (((actor.getWidth()) - 3) * (_actorToRead.getValue("entityDefaultInitilization", "_castingTime") / _actorToRead.getValue("entityDefaultInitilization", "_castingTimeMax"))), (actor.getHeight()));
}

        actor.setAnimation("" + ("" + "aniCastbarFront"));
        actor.drawImage(g);
        /* "Draw attack time indicator below" */
        if(((_actorToRead.getValue("entityDefaultInitilization", "_attackTime") > 0) && (_actorToRead.getValue("entityDefaultInitilization", "_countAttackTime") > 0)))
{
            g.alpha = (45/100);
            g.fillColor = Utils.getColorRGB(255,255,255);
            g.fillRect(0, (actor.getHeight()), (((actor.getWidth()) - 3) * (_actorToRead.getValue("entityDefaultInitilization", "_countAttackTime") / _actorToRead.getValue("entityDefaultInitilization", "_attackTime"))), 2);
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
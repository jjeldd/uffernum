package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_88_88_dummyAi extends ActorScript
{          	
	
public var _DetectPlayerRange:Float;

public var _Phase:String;

public var _targetActor:Actor;

public var _VarList:Array<Dynamic>;

public var _projDamage:Float;
    
/* ========================= Custom Event ========================= */
public function _customEvent_exeInCombat():Void
{
        /* "Get skill info" */
        sayToScene("sceneInit", "_customBlock_skillCast", [actor,asNumber(5)]);
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("Detect Player Range", "_DetectPlayerRange");
_DetectPlayerRange = 360.0;
nameMap.set("Phase", "_Phase");
_Phase = "outCombat";
nameMap.set("targetActor", "_targetActor");
nameMap.set("VarList", "_VarList");
_VarList = [];
nameMap.set("projDamage", "_projDamage");
_projDamage = 15.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        _targetActor = getActor(getGameAttribute("mainplayer"));
propertyChanged("_targetActor", _targetActor);
    
/* ======================== Specific Actor ======================== */
addCollisionListener(actor, function(event:Collision, list:Array<Dynamic>):Void {
if(wrapper.enabled && (_targetActor == event.otherActor)){
        sayToScene("sceneInit", "_customBlock_actorTakeDamage", [_targetActor,5]);
        _targetActor.applyImpulseInDirection(((Utils.DEG * (actor.getAngle()) - 180) + randomInt(Math.floor(-35), Math.floor(35))), 200);
}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        _targetActor = getActor(getGameAttribute("mainplayer"));
propertyChanged("_targetActor", _targetActor);

        if((_Phase == "outCombat"))
{
            actor.enableBehavior("Wander");
}

        else if((_Phase == "inCombat"))
{
            actor.disableBehavior("Wander");
            /* "In Combat Script" */
}

        /* "Switch between in combat and out of combat" */
        if((cast((sayToScene("sceneInit", "_customBlock_distActAct", [actor,_targetActor])), Float) < _DetectPlayerRange))
{
            _Phase = "inCombat";
propertyChanged("_Phase", _Phase);
            actor.shout("_customEvent_" + "exeInCombat");
}

        else
{
            _Phase = "outCombat";
propertyChanged("_Phase", _Phase);
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
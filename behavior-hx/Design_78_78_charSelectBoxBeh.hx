package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_78_78_charSelectBoxBeh extends ActorScript
{          	
	
public var _CharacterIDheld:Float;

public var _CharacterPortrait:String;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("Character ID held", "_CharacterIDheld");
_CharacterIDheld = -1.0;
nameMap.set("Character Portrait", "_CharacterPortrait");
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        g.alpha = (100/100);
        actor.setAnimation("" + ("" + "aniBg"));
        actor.drawImage(g);
        if((hasValue(_CharacterPortrait) != false))
{
            actor.setAnimation("" + _CharacterPortrait);
            actor.drawImage(g);
}

        if(!(actor.isMouseOver()))
{
            actor.setAnimation("" + ("" + "aniBorderNoMouse"));
            actor.drawImage(g);
}

        else
{
            actor.setAnimation("" + ("" + "aniBorderYesMouse"));
            actor.drawImage(g);
}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(actor.isMouseReleased())
{
            trace("" + _CharacterIDheld);
            if(!(_CharacterIDheld == -1))
{
                setGameAttribute("globalIdChosen", _CharacterIDheld);
                switchScene(GameModel.get().scenes.get(0).getID(), createFadeOut((0.5)),createFadeIn((1)));
}

}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
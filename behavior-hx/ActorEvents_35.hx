package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class ActorEvents_35 extends ActorScript
{          	
	
public var _txtColor:Int;

public var _strokeColor:Int;

public var _shadowColor:Int;

public var _text:String;

public var _opacity:Float;

public var _font:Font;

public var _timetoFade:Float;

public var _fadee:Bool;

public var _decSpeed:Float;

public var _size:Float;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("txtColor", "_txtColor");
_txtColor = Utils.getColorRGB(255,255,255);
nameMap.set("strokeColor", "_strokeColor");
_strokeColor = Utils.getColorRGB(0,0,0);
nameMap.set("shadowColor", "_shadowColor");
_shadowColor = Utils.getColorRGB(0,0,0);
nameMap.set("text", "_text");
_text = "Null";
nameMap.set("opacity", "_opacity");
_opacity = 100.0;
nameMap.set("font", "_font");
nameMap.set("timetoFade", "_timetoFade");
_timetoFade = 0.75;
nameMap.set("fadee", "_fadee");
_fadee = false;
nameMap.set("decSpeed", "_decSpeed");
_decSpeed = 5.0;
nameMap.set("size", "_size");
_size = 1.0;

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.disableActorDrawing();

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
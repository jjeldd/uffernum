package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_17_17_floaterBehaviour extends ActorScript
{          	
	
public var _txtColor:Int;

public var _strokeColor:Int;

public var _text:String;

public var _opacity:Float;

public var _timeToFade:Float;

public var _fadee:Bool;

public var _decSpeed:Float;

public var _font:Font;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("txtColor", "_txtColor");
_txtColor = Utils.getColorRGB(255,255,255);
nameMap.set("strokeColor", "_strokeColor");
_strokeColor = Utils.getColorRGB(0,0,0);
nameMap.set("text", "_text");
_text = "Null";
nameMap.set("opacity", "_opacity");
_opacity = 100.0;
nameMap.set("timeToFade", "_timeToFade");
_timeToFade = 0.7;
nameMap.set("fadee", "_fadee");
_fadee = false;
nameMap.set("decSpeed", "_decSpeed");
_decSpeed = 5.0;
nameMap.set("font", "_font");
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.applyImpulseInDirection(randomInt(Math.floor(-145), Math.floor(-35)), 30);
    
/* ======================= After N seconds ======================== */
runLater(1000 * _timeToFade, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        _fadee = true;
propertyChanged("_fadee", _fadee);
}
}, actor);
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        actor.killSelfAfterLeavingScreen();
        if((_fadee == true))
{
            _opacity -= _decSpeed;
propertyChanged("_opacity", _opacity);
}

        if((_opacity <= 0))
{
            recycleActor(actor);
}

}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        g.strokeColor = _txtColor;
        g.fillColor = _strokeColor;
        g.alpha = (_opacity/100);
        g.setFont(getFont(41));
        g.drawString("" + ("" + _text), 0, 0);
}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
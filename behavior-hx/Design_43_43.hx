package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_43_43 extends ActorScript
{          	
	
public var _amt:Float;

public var _timeFlytoPlayer:Float;

public var _DistanceX:Float;

public var _DistanceY:Float;

public var _Distance:Float;

public var _Direction:Float;

public var _Speed:Float;

public var _targetActor:Actor;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("amt", "_amt");
_amt = 10.0;
nameMap.set("timeFlytoPlayer", "_timeFlytoPlayer");
_timeFlytoPlayer = 1.0;
nameMap.set("Distance X", "_DistanceX");
_DistanceX = 0.0;
nameMap.set("Distance Y", "_DistanceY");
_DistanceY = 0.0;
nameMap.set("Distance", "_Distance");
_Distance = 0.0;
nameMap.set("Direction", "_Direction");
_Direction = 0.0;
nameMap.set("Speed", "_Speed");
_Speed = 30.0;
nameMap.set("targetActor", "_targetActor");
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.applyImpulseInDirection(randomInt(Math.floor(-145), Math.floor(-35)), 15);
    
/* ======================= Every N seconds ======================== */
runPeriodically(1000 * 1, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        _timeFlytoPlayer -= 1;
propertyChanged("_timeFlytoPlayer", _timeFlytoPlayer);
}
}, actor);
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((_timeFlytoPlayer <= 0))
{
            if(_targetActor.isAlive())
{
                _DistanceX = asNumber((_targetActor.getXCenter() - actor.getXCenter()));
propertyChanged("_DistanceX", _DistanceX);
                _DistanceY = asNumber((_targetActor.getYCenter() - actor.getYCenter()));
propertyChanged("_DistanceY", _DistanceY);
                _Distance = asNumber(Math.sqrt((Math.pow(_DistanceX, 2) + Math.pow(_DistanceY, 2))));
propertyChanged("_Distance", _Distance);
                _Direction = asNumber(Utils.DEG * (Math.atan2(_DistanceY, _DistanceX)));
propertyChanged("_Direction", _Direction);
                actor.setVelocity(_Direction, _Speed);
}

}

}
});
    
/* ======================== Specific Actor ======================== */
addCollisionListener(actor, function(event:Collision, list:Array<Dynamic>):Void {
if(wrapper.enabled && (_targetActor == event.otherActor)){
        if((_timeFlytoPlayer <= 0))
{
            recycleActor(actor);
}

}
});
    
/* ======================= After N seconds ======================== */
runLater(1000 * 5, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        recycleActor(actor);
}
}, actor);
    
/* ======================== Specific Actor ======================== */
addWhenKilledListener(actor, function(list:Array<Dynamic>):Void {
if(wrapper.enabled){
        sayToScene("sceneInit", "_customBlock_addExp", [_amt,_targetActor]);

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class ActorEvents_172 extends ActorScript
{          	
	
public var _canBeKilled:Bool;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("canBeKilled", "_canBeKilled");
_canBeKilled = false;

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.enableActorDrawing();
        runLater(1000 * 0.5, function(timeTask:TimedTask):Void {
                    for(index0 in 0...Std.int(asNumber(List2D.population_row_or_col("column", Std.int(0), actor.getValue("projInitDefault", "_OnCastApply")))))
{
                        sayToScene("sceneInit", "_customBlock_actorApplyEffect", [actor.getValue("projInitDefault", "_Owner"),asNumber(List2D.get_entry(Std.int(0), Std.int(index0), actor.getValue("projInitDefault", "_OnCastApply"))),asNumber(List2D.get_entry(Std.int(1), Std.int(index0), actor.getValue("projInitDefault", "_OnCastApply")))]);
}

}, actor);
    
/* ======================= After N seconds ======================== */
runLater(1000 * 0.05, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        recycleActor(actor);
}
}, actor);
    
/* ======================== Something Else ======================== */
addCollisionListener(actor, function(event:Collision, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((event.otherActor.getGroup() == getActorGroup(7)))
{
            sayToScene("sceneInit", "_customBlock_actorTakeDamage", [event.otherActor,actor.getValue("projInitDefault", "_Damage")]);
            sayToScene("sceneInit", "_customBlock_takeTrueDamageGlobal", [event.otherActor,actor.getValue("projInitDefault", "_TrueDamage")]);
            if(event.otherActor.getValue("entityDefaultInitilization", "_CanBeKnockback"))
{
                event.otherActor.applyImpulseInDirection(((Utils.DEG * (actor.getAngle()) - 180) + randomInt(Math.floor(-35), Math.floor(35))), actor.getValue("projInitDefault", "_KnockbackForce"));
}

            if(true)
{
                for(index0 in 0...Std.int(asNumber(List2D.population_row_or_col("column", Std.int(0), actor.getValue("projInitDefault", "_OnHitApply")))))
{
                    sayToScene("sceneInit", "_customBlock_actorApplyEffect", [event.otherActor,asNumber(List2D.get_entry(Std.int(0), Std.int(index0), actor.getValue("projInitDefault", "_OnHitApply"))),asNumber(List2D.get_entry(Std.int(1), Std.int(index0), actor.getValue("projInitDefault", "_OnHitApply")))]);
}

}

            recycleActor(actor);
}

        if((_canBeKilled && (event.otherActor.getGroup() == getActorGroup(1))))
{
            createRecycledActor(getActorType(112), (actor.getX() + 8), actor.getY(), Script.BACK);
            getLastCreatedActor().setAngle(Utils.RAD * (Utils.DEG * (actor.getAngle())));
            recycleActor(actor);
}

        if((_canBeKilled && (event.otherActor.getGroup() == getActorGroup(6))))
{
            if(!(event.otherActor.getType() == event.thisActor.getType()))
{
                recycleActor(actor);
}

}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
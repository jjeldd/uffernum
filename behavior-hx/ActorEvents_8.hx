package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class ActorEvents_8 extends ActorScript
{          	
	
public var _skillCol:Float;

public var _effCol:Float;

public var _demoTimer:Float;

public var _Phase:String;
    
/* ========================= Custom Block ========================= */


/* Params are: __Name __Description __RechargeTime __Cost __ResourceName __CastTime */
public function _customBlock_addSpellToDB(__Name:String, __Description:String, __RechargeTime:Float, __Cost:Float, __ResourceName:String, __CastTime:Float):Float
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(0), Std.int(getGameAttribute("noSpells")), getGameAttribute("noSpells"), getGameAttribute("skillList"));
        List2D.set_entry(Std.int(1), Std.int(getGameAttribute("noSpells")), __Name, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(2), Std.int(getGameAttribute("noSpells")), __Description, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(6), Std.int(getGameAttribute("noSpells")), __RechargeTime, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(7), Std.int(getGameAttribute("noSpells")), __Cost, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(8), Std.int(getGameAttribute("noSpells")), __ResourceName, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(9), Std.int(getGameAttribute("noSpells")), __CastTime, getGameAttribute("skillList"));

        setGameAttribute("noSpells", (getGameAttribute("noSpells") + 1));
        return (getGameAttribute("noSpells") - 1);
}
    
/* ========================= Custom Block ========================= */


/* Params are: __EffectID __imageName */
public function _customBlock_setEffectImage(__EffectID:Float, __imageName:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(6), Std.int(__EffectID), __imageName, getGameAttribute("maineffectsList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __spellid __image */
public function _customBlock_setimageSpell(__spellid:Float, __image:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(10), Std.int(__spellid), __image, getGameAttribute("skillList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __name __desc */
public function _customBlock_addCharacter(__name:String, __desc:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(0), Std.int(getGameAttribute("noCharacters")), getGameAttribute("noCharacters"), getGameAttribute("entityList"));
        List2D.set_entry(Std.int(1), Std.int(getGameAttribute("noCharacters")), __name, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(2), Std.int(getGameAttribute("noCharacters")), __desc, getGameAttribute("entityList"));
        setGameAttribute("noCharacters", (getGameAttribute("noCharacters") + 1));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __ID __hp __mp __str __agi __int */
public function _customBlock_setMainStats(__ID:Float, __hp:Float, __mp:Float, __str:Float, __agi:Float, __int:Float):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(3), Std.int(__ID), __hp, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(4), Std.int(__ID), __mp, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(5), Std.int(__ID), __str, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(6), Std.int(__ID), __agi, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(7), Std.int(__ID), __int, getGameAttribute("entityList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __id __vit __luck __def */
public function _customBlock_setSecondaryStats(__id:Float, __vit:Float, __luck:Float, __def:Float):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(8), Std.int(__id), __vit, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(9), Std.int(__id), __luck, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(10), Std.int(__id), __def, getGameAttribute("entityList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __id __left __right */
public function _customBlock_setCharacterAttacks(__id:Float, __left:String, __right:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(20), Std.int(__id), __left, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(21), Std.int(__id), __right, getGameAttribute("entityList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __ID __abi1 __abi2 __abi3 __abi4 __abi5 */
public function _customBlock_setCharacterAbilities(__ID:Float, __abi1:Float, __abi2:Float, __abi3:Float, __abi4:Float, __abi5:Float):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(15), Std.int(__ID), __abi1, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(16), Std.int(__ID), __abi2, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(17), Std.int(__ID), __abi3, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(18), Std.int(__ID), __abi4, getGameAttribute("entityList"));
        List2D.set_entry(Std.int(19), Std.int(__ID), __abi5, getGameAttribute("entityList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __id __R __G __B */
public function _customBlock_setColorOfEffect(__id:Float, __R:Float, __G:Float, __B:Float):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(8), Std.int(__id), __R, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(9), Std.int(__id), __G, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(10), Std.int(__id), __B, getGameAttribute("maineffectsList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __slot __scrsec */
public function _customBlock_seceffscr(__slot:Float, __scrsec:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(7), Std.int(__slot), __scrsec, getGameAttribute("maineffectsList"));
}
    
/* ========================= Custom Block ========================= */


/* Params are: __name __desc __scrb __scrd __scre */
public function _customBlock_addeff(__name:String, __desc:String, __scrb:String, __scrd:String, __scre:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(0), Std.int(getGameAttribute("noEffects")), getGameAttribute("noEffects"), getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(1), Std.int(getGameAttribute("noEffects")), __name, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(2), Std.int(getGameAttribute("noEffects")), __desc, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(3), Std.int(getGameAttribute("noEffects")), __scrb, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(4), Std.int(getGameAttribute("noEffects")), __scrd, getGameAttribute("maineffectsList"));
        List2D.set_entry(Std.int(5), Std.int(getGameAttribute("noEffects")), __scre, getGameAttribute("maineffectsList"));
        setGameAttribute("noEffects", (getGameAttribute("noEffects") + 1));

}
    
/* ========================= Custom Block ========================= */


/* Params are: __SpellID __Scriptbegin __ScriptDuring __ScriptEnd */
public function _customBlock_setScripts(__SpellID:Float, __Scriptbegin:String, __ScriptDuring:String, __ScriptEnd:String):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(3), Std.int(__SpellID), __Scriptbegin, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(4), Std.int(__SpellID), __ScriptDuring, getGameAttribute("skillList"));
        List2D.set_entry(Std.int(5), Std.int(__SpellID), __ScriptEnd, getGameAttribute("skillList"));
}

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("skillCol", "_skillCol");
_skillCol = 15.0;
nameMap.set("effCol", "_effCol");
_effCol = 15.0;
nameMap.set("demoTimer", "_demoTimer");
_demoTimer = 60.0;
nameMap.set("Phase", "_Phase");
_Phase = "";

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        actor.anchorToScreen();
        actor.disableActorDrawing();
        actor.makeAlwaysSimulate();
        _Phase = "Loading";
propertyChanged("_Phase", _Phase);
    
/* ======================== When Creating ========================= */
        _Phase = "Initialize skill database";
propertyChanged("_Phase", _Phase);
        trace("" + "Initialize skill database");
        setGameAttribute("skillList", List2D.new_2D(Std.int(10), Std.int(_skillCol)));
        /* "Set basic Variables for each slot" */
        /* "Check SkillbarList.txt for help" */
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", getGameAttribute("skillList"))))
{

            List2D.set_entry(Std.int(0), Std.int(index0), -1, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(1), Std.int(index0), "Null", getGameAttribute("skillList"));
            List2D.set_entry(Std.int(2), Std.int(index0), "Null", getGameAttribute("skillList"));
            List2D.set_entry(Std.int(3), Std.int(index0), -1, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(4), Std.int(index0), -1, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(5), Std.int(index0), -1, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(6), Std.int(index0), 0, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(7), Std.int(index0), 0, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(8), Std.int(index0), "Null", getGameAttribute("skillList"));
            List2D.set_entry(Std.int(9), Std.int(index0), 0, getGameAttribute("skillList"));
            List2D.set_entry(Std.int(10), Std.int(index0), -1, getGameAttribute("skillList"));

}

    
/* ======================== When Creating ========================= */
        _Phase = "Initialize Entity Database";
propertyChanged("_Phase", _Phase);
        trace("" + "Initialize Entity Database");
        setGameAttribute("entityList", List2D.new_2D(Std.int(25), Std.int(25)));
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", getGameAttribute("entityList"))))
{

            List2D.set_entry(Std.int(0), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(1), Std.int(index0), "Null", getGameAttribute("entityList"));
            List2D.set_entry(Std.int(2), Std.int(index0), "Null", getGameAttribute("entityList"));
            List2D.set_entry(Std.int(3), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(4), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(5), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(6), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(7), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(8), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(9), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(10), Std.int(index0), 1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(11), Std.int(index0), 17.5, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(12), Std.int(index0), 225.0, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(13), Std.int(index0), 1.3, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(14), Std.int(index0), 0, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(15), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(16), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(17), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(18), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(19), Std.int(index0), -1, getGameAttribute("entityList"));
            List2D.set_entry(Std.int(20), Std.int(index0), "None", getGameAttribute("entityList"));
            List2D.set_entry(Std.int(21), Std.int(index0), "None", getGameAttribute("entityList"));

}

    
/* ======================== When Creating ========================= */
        _Phase = "Initialize effects database";
propertyChanged("_Phase", _Phase);
        trace("" + "Initialize effects database");
        setGameAttribute("maineffectsList", List2D.new_2D(Std.int(12), Std.int(_effCol)));
        /* "Set basic Variables for each slot" */
        /* "Check EffectList.txt for help" */
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", getGameAttribute("maineffectsList"))))
{

            List2D.set_entry(Std.int(0), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(1), Std.int(index0), "Null", getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(2), Std.int(index0), "Null", getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(3), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(4), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(5), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(6), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(7), Std.int(index0), -1, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(8), Std.int(index0), 255, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(9), Std.int(index0), 255, getGameAttribute("maineffectsList"));
            List2D.set_entry(Std.int(10), Std.int(index0), 255, getGameAttribute("maineffectsList"));
}

    
/* ======================== When Creating ========================= */
        trace("" + "Loading Characters");
        _Phase = "Loading Characters";
propertyChanged("_Phase", _Phase);
        actor.say("ActorEvents_8", "_customBlock_addCharacter", ["Novaria","Whirlwind Ranger"]);
        actor.say("ActorEvents_8", "_customBlock_setMainStats", [0,120,50,5,8,3]);
        actor.say("ActorEvents_8", "_customBlock_setSecondaryStats", [0,3,4,6]);
        actor.say("ActorEvents_8", "_customBlock_setCharacterAbilities", [0,0,1,2,3,4]);
        actor.say("ActorEvents_8", "_customBlock_setCharacterAttacks", [0,"novariaLeftClickMelee","novariaRightClickArrow"]);
        /* "----------------------------------------------------------------------------------------------------------------" */
        actor.say("ActorEvents_8", "_customBlock_addCharacter", ["NO NAME COS DAVE SUCKS","Shinobi Shit cos no description"]);
        actor.say("ActorEvents_8", "_customBlock_setMainStats", [1,100,75,3,8,5]);
        actor.say("ActorEvents_8", "_customBlock_setSecondaryStats", [1,5,4,4]);
        actor.say("ActorEvents_8", "_customBlock_setCharacterAbilities", [1,-1,-1,-1,-1,-1]);
        actor.say("ActorEvents_8", "_customBlock_setCharacterAttacks", [1,"None","None"]);
    
/* ======================== When Creating ========================= */
        _Phase = "Loading spells";
propertyChanged("_Phase", _Phase);
        trace("" + "Loading spells");
        trace("" + "Player Spells");
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Swift Arrows","You are unable to melee but your attack speed is increased by 100%. You also move 50% faster.",12,5,"Donal",0.2])), Float),"None","None","skillEndSwiftArrows"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [0,"spellIcn/spellIcn1.png"]);
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Cyclone Blast","Knocksback any close by enemies and does minimal damage to them.",15,10,"Donal",0.5])), Float),"None","None","skillEndCycloneBlast"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [1,"spellIcn/spellIcn2.png"]);
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Splinted Arrows","Fires a volley of arrows directly infront of you.",1,10,"Donal",0.5])), Float),"None","None","skillEndSplintedArrows"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [2,"spellIcn/spellIcn3.png"]);
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Stunning Blow","You fire an extremely fast arrow which stuns enemies in a line for 0.5 seconds. The first target hit is stunned for a further 1 second. ",5,15,"Donal",1])), Float),"None","None","skillEndStunningArrow"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [3,"spellIcn/spellIcn4.png"]);
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Goddesses Poison","You fire an extremely large arrow. Enemies hit will take 200% damage and poisoned.",5,20,"Donal",1])), Float),"None","None","skillEndGoddessesPoison"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [4,"spellIcn/spellIcn4.png"]);
        trace("" + "Enemy Abilities");
        actor.say("ActorEvents_8", "_customBlock_setScripts", [cast((actor.say("ActorEvents_8", "_customBlock_addSpellToDB", ["Charge","[ENEMY SKILL] After a time you will charge in a direction",5,15,"Donal",2])), Float),"None","None","enemyChargeEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setimageSpell", [5,"spellIcn/spellIcn4.png"]);
    
/* ======================== When Creating ========================= */
        _Phase = "Loading Effects";
propertyChanged("_Phase", _Phase);
        trace("" + "Loading Effects");
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Burning","You are taking damage overtime","effBurningCreate","effBurningDuring","effBurningEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [0,"effectIcn/burningIcn.png"]);
        actor.say("ActorEvents_8", "_customBlock_seceffscr", [0,"effBurningSec"]);
        actor.say("ActorEvents_8", "_customBlock_setColorOfEffect", [0,204,0,0]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Haste","You gain an increase in movement speed","effHasteCreate","none","effHasteEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [1,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Slowed","You are slowed!","effSlowedCreate","effSlowedDuring","effSlowedEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [2,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Silenced","You are silenced!","none","effSilencedDuring","effSilencedEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [3,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Stunned","You are stunned!","none","effStunnedDuring","effStunnedEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [4,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Blind","You are blinded!","none","effBlindDuring","none"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [5,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Springy","You can jump higher!","effSpringyCreate","none","effSpringyEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [6,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Regen","Healing overtime!","none","none","none"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [7,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_seceffscr", [7,"effRegenSec"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Curse","Reduced defence.","effCurseCreate","none","effCurseEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [8,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Magic Shield","Increased defence. ","effMagicShieldCreate","none","effMagicShieldEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [9,"effectIcn/effectTester2.png"]);
        actor.say("ActorEvents_8", "_customBlock_setColorOfEffect", [9,153,51,153]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Potion!","Healing a percentage of health overtime. ","none","effPotionDuring","none"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [10,"effectIcn/potionIcn.png"]);
        actor.say("ActorEvents_8", "_customBlock_addeff", ["Swift Arrows","Increased attack and movement speed.","effSwiftArrowsCreate","none","effSwiftArrowsEnd"]);
        actor.say("ActorEvents_8", "_customBlock_setEffectImage", [11,"effectIcn/swiftArrows.png"]);
        _Phase = "Done!";
propertyChanged("_Phase", _Phase);
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((_Phase == ("" + "Done!")))
{
            switchScene(GameModel.get().scenes.get(2).getID(), createFadeOut((0.2)),createFadeIn((1)));
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
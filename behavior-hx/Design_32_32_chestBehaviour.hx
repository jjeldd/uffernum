package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_32_32_chestBehaviour extends ActorScript
{          	
	
public var _amtDonal:Float;

public var _itemList:Array<Dynamic>;

public var _amtExp:Float;

public var _openBool:Bool;

public var _minDonal:Float;

public var _maxDonal:Float;

public var _amtKeys:Float;

public var _Locked:Bool;

public var _chanceKey:Float;
    
/* ========================= Custom Event ========================= */
public function _customEvent_chestOpen():Void
{
        /* "Drop Gold" */
        _amtDonal = asNumber((_amtDonal * (1 + getActor(getGameAttribute("mainplayer")).getValue("entityDefaultInitilization", "_GOLDFIND"))));
propertyChanged("_amtDonal", _amtDonal);
        if((_amtDonal > 0))
{
            sayToScene("sceneInit", "_customBlock_createGold", [_amtDonal,actor.getX(),actor.getY(),Math.round((_amtDonal / randomInt(Math.floor(_minDonal), Math.floor(_maxDonal))))]);
}

        /* "Drop Key" */
        if((_amtKeys > 0))
{
            sayToScene("sceneInit", "_customBlock_keyCreate", [_amtKeys,actor.getX(),actor.getY(),_chanceKey]);
}

        /* "Drop Exp" */
        _amtExp = asNumber((_amtExp * (1 + getActor(getGameAttribute("mainplayer")).getValue("entityDefaultInitilization", "_GOLDFIND"))));
propertyChanged("_amtExp", _amtExp);
        if((_amtExp > 0))
{
            sayToScene("sceneInit", "_customBlock_createExpOrb", [_amtExp,actor.getX(),actor.getY(),Math.round((_amtExp / randomInt(Math.floor(_minDonal), Math.floor(_maxDonal))))]);
}

        _openBool = true;
propertyChanged("_openBool", _openBool);
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("amtDonal", "_amtDonal");
_amtDonal = 0.0;
nameMap.set("itemList", "_itemList");
_itemList = [];
nameMap.set("amtExp", "_amtExp");
_amtExp = 0.0;
nameMap.set("openBool", "_openBool");
_openBool = false;
nameMap.set("minDonal", "_minDonal");
_minDonal = 3.0;
nameMap.set("maxDonal", "_maxDonal");
_maxDonal = 5.0;
nameMap.set("amtKeys", "_amtKeys");
_amtKeys = 0.0;
nameMap.set("Locked", "_Locked");
_Locked = false;
nameMap.set("chanceKey", "_chanceKey");
_chanceKey = 1.0;
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */

    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(_openBool)
{
            actor.setAnimation("" + ("" + "chestOpen"));
}

        else
{
            actor.setAnimation("" + ("" + "chestDefault"));
}

}
});
    
/* ======================= Member of Group ======================== */
addCollisionListener(actor, function(event:Collision, list:Array<Dynamic>):Void {
if(wrapper.enabled && sameAsAny(getActorGroup(0),event.otherActor.getType(),event.otherActor.getGroup())){
        if((!(_openBool) && !(_Locked)))
{
            actor.say("chestBehaviour", "_customEvent_" + "chestOpen");
            playSound(getSound(200));
}

        else if((!(_openBool) && _Locked))
{
            if((getGameAttribute("currentKeys") > 0))
{
                setGameAttribute("currentKeys", (getGameAttribute("currentKeys") - 1));
                playSound(getSound(200));
                _Locked = false;
propertyChanged("_Locked", _Locked);
}

}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
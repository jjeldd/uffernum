package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_36_36_sceneInit extends SceneScript
{          	
	
public var _amtCreate:Float;

public var _damageMultiplier:Float;

public var _colList:Array<Dynamic>;

public var _cast:Bool;

public var _VarList:Array<Dynamic>;

public var _maxColumns:Float;

public var _DistanceX:Float;

public var _DistanceY:Float;

public var _tempSlot:Float;

public var _effectsList:Array<Dynamic>;

public var _tempeffNum:Float;

public var _tempSec:Float;

public var _effTrigbegin:String;
    
/* ========================= Custom Block ========================= */


/* Params are:__amtg __xx __yy __splitamt */
public function _customBlock_createGold(__amtg:Float, __xx:Float, __yy:Float, __splitamt:Float):Void
{
        _amtCreate = asNumber(Math.round((__amtg / __splitamt)));
propertyChanged("_amtCreate", _amtCreate);
        for(index0 in 0...Std.int(_amtCreate))
{
            createRecycledActor(getActorType(75), __xx, __yy, Script.FRONT);
            getLastCreatedActor().setValue("donalBehaviour", "_amt", __splitamt);
            getLastCreatedActor().setValue("donalBehaviour", "_targetActor", getActor(getGameAttribute("mainplayer")));
}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__value1 __value2 __factor */
public function _customBlock_lerpVal(__value1:Float, __value2:Float, __factor:Float):Float
{
        return (__value1 + ((__value2 - __value1) * __factor));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__actorOne __actorTwo */
public function _customBlock_distActAct(__actorOne:Actor, __actorTwo:Actor):Float
{
        return Math.sqrt((Math.pow((__actorOne.getYCenter() - __actorTwo.getYCenter()), 2) + Math.pow((__actorOne.getXCenter() - __actorTwo.getXCenter()), 2)));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__actor __damage */
public function _customBlock_takeTrueDamageGlobal(__actor:Actor, __damage:Float):Void
{
        if((__damage > 0))
{
            /* "For taking true damage given to actor" */
            if((__actor.getValue("entityDefaultInitilization", "_invulnerable") == false))
{
                __actor.setValue("entityDefaultInitilization", "_health", (__actor.getValue("entityDefaultInitilization", "_health") - __damage));
                __actor.setValue("entityDefaultInitilization", "_invulCount", __actor.getValue("entityDefaultInitilization", "_invulSeconds"));

}

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__actor __damage */
public function _customBlock_actorTakeDamage(__actor:Actor, __damage:Float):Void
{
        if((__damage > 0))
{
            if((__actor.getValue("entityDefaultInitilization", "_invulnerable") == false))
{
                /* "Calculate damage multiplier" */
                _damageMultiplier = asNumber(__actor.getValue("entityDefaultInitilization", "_damageMultiplier"));
propertyChanged("_damageMultiplier", _damageMultiplier);
                __actor.setValue("entityDefaultInitilization", "_health", (__actor.getValue("entityDefaultInitilization", "_health") - (__damage * _damageMultiplier)));
                __actor.setValue("entityDefaultInitilization", "_invulCount", __actor.getValue("entityDefaultInitilization", "_invulSeconds"));

}

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__actor __amt */
public function _customBlock_actorHeal(__actor:Actor, __amt:Float):Void
{
        if((__amt > 0))
{
            /* "Calculate damage multiplier" */
            __actor.setValue("entityDefaultInitilization", "_health", (__actor.getValue("entityDefaultInitilization", "_health") + __amt));

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__actor __mana */
public function _customBlock_actorTakeMana(__actor:Actor, __mana:Float):Void
{
        if((__mana > 0))
{
            /* "Used for taking/adding mana from/to an entity" */
            __actor.setValue("entityDefaultInitilization", "_mana", (__actor.getValue("entityDefaultInitilization", "_mana") - __mana));


}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__x1 __y1 __x2 __y2 */
public function _customBlock_retXYdegree(__x1:Float, __y1:Float, __x2:Float, __y2:Float):Float
{
        _DistanceX = asNumber(((getScreenX() + __x2) - __x1));
propertyChanged("_DistanceX", _DistanceX);
        _DistanceY = asNumber(((getScreenY() + __y2) - __y1));
propertyChanged("_DistanceY", _DistanceY);
        return Utils.DEG * (Math.atan2(_DistanceY, _DistanceX));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__amount __xx __yy __split */
public function _customBlock_createExpOrb(__amount:Float, __xx:Float, __yy:Float, __split:Float):Void
{
        _amtCreate = asNumber(Math.round((__amount / __split)));
propertyChanged("_amtCreate", _amtCreate);
        for(index0 in 0...Std.int(_amtCreate))
{
            createRecycledActor(getActorType(93), __xx, __yy, Script.FRONT);
            getLastCreatedActor().setValue("expBehaviour", "_amt", __split);
            getLastCreatedActor().setValue("expBehaviour", "_targetActor", getActor(getGameAttribute("mainplayer")));
}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__expAmt __Actor */
public function _customBlock_addExp(__expAmt:Float, __Actor:Actor):Void
{
        __Actor.setValue("entityDefaultInitilization", "_curExp", (__Actor.getValue("entityDefaultInitilization", "_curExp") + __expAmt));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__amt */
public function _customBlock_expaddPlayer(__amt:Float):Void
{
        getActor(getGameAttribute("mainplayer")).setValue("entityDefaultInitilization", "_curExp", (getActor(getGameAttribute("mainplayer")).getValue("entityDefaultInitilization", "_curExp") + __amt));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__act */
public function _customBlock_actID(__act:Actor):Float
{
        return __act.ID;
}
    
/* ========================= Custom Block ========================= */


/* Params are:__amt __xx __yy __perc */
public function _customBlock_keyCreate(__amt:Float, __xx:Float, __yy:Float, __perc:Float):Void
{
        _amtCreate = asNumber(Math.round(__amt));
propertyChanged("_amtCreate", _amtCreate);
        for(index0 in 0...Std.int(_amtCreate))
{
            if(cast((sayToScene("sceneInit", "_customBlock_chancePerc", [__perc])), Bool))
{
                createRecycledActor(getActorType(86), __xx, __yy, Script.FRONT);
}

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__xx __yy __txt */
public function _customBlock_createFloater(__xx:Float, __yy:Float, __txt:String):Void
{
        createRecycledActor(getActorType(35), __xx, __yy, Script.FRONT);
        getLastCreatedActor().setValue("floaterBehaviour", "_text", ("" + __txt));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__chancePerc */
public function _customBlock_chancePerc(__chancePerc:Float):Bool
{
        if((randomFloat() <= __chancePerc))
{
            return true;
}

        return false;
}
    
/* ========================= Custom Block ========================= */


/* Params are:__actor __skillID */
public function _customBlock_skillCast(__actor:Actor, __skillID:Float):Void
{
        _cast = __actor.getValue("entityDefaultInitilization", "_canCast");
propertyChanged("_cast", _cast);
        if(_cast)
{
            if(((__actor.getValue("entityDefaultInitilization", "_mana") >= List2D.get_entry(Std.int(7), Std.int(__skillID), getGameAttribute("skillList"))) && __actor.getValue("entityDefaultInitilization", "_finishCasting")))
{
                __actor.setValue("entityDefaultInitilization", "_castingBarAlpha", 1);
                __actor.setValue("entityDefaultInitilization", "_castingSpell", __skillID);
                __actor.setValue("entityDefaultInitilization", "_castingTimeMax", List2D.get_entry(Std.int(9), Std.int(__skillID), getGameAttribute("skillList")));
                __actor.setValue("entityDefaultInitilization", "_finishCasting", false);
                if((!(List2D.get_entry(Std.int(3), Std.int(__skillID), getGameAttribute("skillList")) == "None") && !(asNumber(List2D.get_entry(Std.int(3), Std.int(__skillID), getGameAttribute("skillList"))) == -1)))
{
                    __actor.shout("_customEvent_" + List2D.get_entry(Std.int(3), Std.int(__skillID), getGameAttribute("skillList")));
}

}

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__x1 __y1 __x2 __y2 */
public function _customBlock_mouseInArea(__x1:Float, __y1:Float, __x2:Float, __y2:Float):Bool
{
        return ((getMouseX() > __x1) && ((getMouseX() < __x2) && ((getMouseY() > __y1) && (getMouseY() < __y2))));
}
    
/* ========================= Custom Block ========================= */


/* Params are:__Actor __EffectID __Seconds */
public function _customBlock_actorApplyEffect(__Actor:Actor, __EffectID:Float, __Seconds:Float):Void
{
        _tempeffNum = asNumber(cast((sayToScene("sceneInit", "_customBlock_CheckForEffect", [__Actor,__EffectID])), Float));
propertyChanged("_tempeffNum", _tempeffNum);
        if(!(_tempeffNum == -1))
{
            _tempSec = asNumber(List2D.get_entry(Std.int(1), Std.int(_tempeffNum), __Actor.getValue("entityDefaultInitilization", "_effectsList")));
propertyChanged("_tempSec", _tempSec);
            if((__Seconds > _tempSec))
{
                /* "Reapply effect with full duration" */
                List2D.set_entry(Std.int(0), Std.int(_tempeffNum), __EffectID, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
                List2D.set_entry(Std.int(1), Std.int(_tempeffNum), __Seconds, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
                List2D.set_entry(Std.int(2), Std.int(_tempeffNum), __Seconds, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
}

}

        else
{
            for(index0 in 0...Std.int(List2D.check_dimensions("rows", _effectsList)))
{
                _tempeffNum = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), __Actor.getValue("entityDefaultInitilization", "_effectsList")));
propertyChanged("_tempeffNum", _tempeffNum);
                if((_tempeffNum == -1))
{
                    List2D.set_entry(Std.int(0), Std.int(index0), __EffectID, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
                    List2D.set_entry(Std.int(1), Std.int(index0), __Seconds, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
                    List2D.set_entry(Std.int(2), Std.int(index0), __Seconds, __Actor.getValue("entityDefaultInitilization", "_effectsList"));
                    /* "Execute creation script" */
                    _effTrigbegin = ("" + List2D.get_entry(Std.int(3), Std.int(__EffectID), getGameAttribute("maineffectsList")));
propertyChanged("_effTrigbegin", _effTrigbegin);
                    if(!(_effTrigbegin == ("" + "none")))
{

                        __Actor.shout("_customEvent_" + _effTrigbegin);
}

                    break;
}

}

}

}
    
/* ========================= Custom Block ========================= */


/* Params are:__Actor __EffectID */
public function _customBlock_CheckForEffect(__Actor:Actor, __EffectID:Float):Float
{
        _effectsList = List2D.copy_2D(__Actor.getValue("entityDefaultInitilization", "_effectsList"));
propertyChanged("_effectsList", _effectsList);
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", _effectsList)))
{
            _tempSlot = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), _effectsList));
propertyChanged("_tempSlot", _tempSlot);
            if((__EffectID == _tempSlot))
{
                return index0;
}

}

        return -1;
}
    
/* ========================= Custom Block ========================= */


/* Params are:__Actor __Damage __TrueDamage __Owner __KnockbackForce */
public function _customBlock_setProjVariables(__Actor:Actor, __Damage:Float, __TrueDamage:Float, __Owner:Actor, __KnockbackForce:Float):Void
{
        __Actor.setValue("projInitDefault", "_Damage", __Damage);
        __Actor.setValue("projInitDefault", "_TrueDamage", __TrueDamage);
        __Actor.setValue("projInitDefault", "_Owner", __Owner);
        __Actor.setValue("projInitDefault", "_KnockbackForce", __KnockbackForce);
}

 
 	public function new(dummy:Int, engine:Engine)
	{
		super(engine);
		nameMap.set("amtCreate", "_amtCreate");
_amtCreate = 0.0;
nameMap.set("damageMultiplier", "_damageMultiplier");
_damageMultiplier = 0.0;
nameMap.set("colList", "_colList");
_colList = [];
nameMap.set("cast", "_cast");
_cast = false;
nameMap.set("VarList", "_VarList");
_VarList = [];
nameMap.set("maxColumns", "_maxColumns");
_maxColumns = 10.0;
nameMap.set("Distance X", "_DistanceX");
_DistanceX = 0.0;
nameMap.set("Distance Y", "_DistanceY");
_DistanceY = 0.0;
nameMap.set("tempSlot", "_tempSlot");
_tempSlot = 0.0;
nameMap.set("effectsList", "_effectsList");
_effectsList = [];
nameMap.set("tempeffNum", "_tempeffNum");
_tempeffNum = 0.0;
nameMap.set("tempSec", "_tempSec");
_tempSec = 0.0;
nameMap.set("effTrigbegin", "_effTrigbegin");
_effTrigbegin = "";

	}
	
	override public function init()
	{
		
	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_30_30_drawSkillbar extends ActorScript
{          	
	
public var _VarList:Array<Dynamic>;

public var _maxColumns:Float;

public var _ranDebug:Float;

public var _actorToRead:Actor;

public var _ranBool:Bool;

public var _imageInstList:Array<Dynamic>;

public var _imageList:Array<Dynamic>;

public var _tempImg:BitmapData;

public var _tempImgInst:Bitmap;

public var _tempNum:Float;

public var _gapAftereachSkill:Float;

public var _tempBool:Bool;

public var _boolList:Array<Dynamic>;

public var _tempList:Array<Dynamic>;

public var _numInSlot:Float;

public var _TopImageInst:Bitmap;

public var _TopImage:BitmapData;

public var _topImageInstList:Array<Dynamic>;

public var _topDrawn:Bool;

public var _cooldownImageInst:Array<Dynamic>;

public var _cooldownImage:Array<Dynamic>;

public var _tempNum2:Float;

public var _ListBool2:Array<Dynamic>;

public var _oldcdcd:Float;

public var _tempNum3:Float;

public var _woodTableInst:Bitmap;

public var _tempNum4:Float;

public var _tempxx:Float;

public var _tempyy:Float;
    
/* ========================= Custom Block ========================= */


/* Params are: __slot __abiNumber */
public function _customBlock_setSlotAbi(__slot:Float, __abiNumber:Float):Void
{
var __Self:Actor = actor;
        if(!(asNumber(List2D.get_entry(Std.int(2), Std.int(__slot), _VarList)) == __abiNumber))
{
            List2D.set_entry(Std.int(2), Std.int(__slot), __abiNumber, _VarList);

            List2D.set_entry(Std.int(5), Std.int(__slot), List2D.get_entry(Std.int(6), Std.int(__abiNumber), getGameAttribute("skillList")), _VarList);
            trace("" + __abiNumber);
}

}
    
/* ========================= Custom Block ========================= */


/* Params are: __slot */
public function _customBlock_autoSetSlotCd(__slot:Float):Void
{
var __Self:Actor = actor;
        List2D.set_entry(Std.int(4), Std.int(__slot), List2D.get_entry(Std.int(5), Std.int(__slot), _VarList), _VarList);
}

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("VarList", "_VarList");
_VarList = [];
nameMap.set("maxColumns", "_maxColumns");
_maxColumns = 10.0;
nameMap.set("ranDebug", "_ranDebug");
_ranDebug = 0.0;
nameMap.set("actorToRead", "_actorToRead");
nameMap.set("ranBool", "_ranBool");
_ranBool = true;
nameMap.set("imageInstList", "_imageInstList");
_imageInstList = [];
nameMap.set("imageList", "_imageList");
_imageList = [];
nameMap.set("tempImg", "_tempImg");
nameMap.set("tempImgInst", "_tempImgInst");
nameMap.set("tempNum", "_tempNum");
_tempNum = 0.0;
nameMap.set("gapAftereachSkill", "_gapAftereachSkill");
_gapAftereachSkill = 72.0;
nameMap.set("Actor", "actor");
nameMap.set("tempBool", "_tempBool");
_tempBool = false;
nameMap.set("boolList", "_boolList");
_boolList = [];
nameMap.set("tempList", "_tempList");
_tempList = [];
nameMap.set("numInSlot", "_numInSlot");
_numInSlot = 0.0;
nameMap.set("TopImageInst", "_TopImageInst");
nameMap.set("TopImage", "_TopImage");
nameMap.set("topImageInstList", "_topImageInstList");
_topImageInstList = [];
nameMap.set("topDrawn", "_topDrawn");
_topDrawn = false;
nameMap.set("cooldownImageInst", "_cooldownImageInst");
_cooldownImageInst = [];
nameMap.set("cooldownImage", "_cooldownImage");
_cooldownImage = [];
nameMap.set("tempNum2", "_tempNum2");
_tempNum2 = 0.0;
nameMap.set("ListBool2", "_ListBool2");
_ListBool2 = [];
nameMap.set("oldcdcd", "_oldcdcd");
_oldcdcd = 0.0;
nameMap.set("tempNum3", "_tempNum3");
_tempNum3 = 0.0;
nameMap.set("woodTableInst", "_woodTableInst");
nameMap.set("tempNum4", "_tempNum4");
_tempNum4 = 0.0;
nameMap.set("tempxx", "_tempxx");
_tempxx = 0.0;
nameMap.set("tempyy", "_tempyy");
_tempyy = 0.0;

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        _VarList = List2D.new_2D(Std.int(getGameAttribute("noSkillslots")), Std.int(_maxColumns));
propertyChanged("_VarList", _VarList);
        /* "Set basic Variables for each slot" */
        /* "Check SkillbarList.txt for help" */
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{

            List2D.set_entry(Std.int(0), Std.int(index0), 0, _VarList);
            List2D.set_entry(Std.int(1), Std.int(index0), 1, _VarList);
            List2D.set_entry(Std.int(2), Std.int(index0), -1, _VarList);
            List2D.set_entry(Std.int(3), Std.int(index0), 0, _VarList);
            List2D.set_entry(Std.int(4), Std.int(index0), 0, _VarList);
            List2D.set_entry(Std.int(5), Std.int(index0), 0, _VarList);

}

        /* "Set keyboard keys" */
        List2D.set_entry(Std.int(3), Std.int(0), 49, _VarList);
        List2D.set_entry(Std.int(3), Std.int(1), 50, _VarList);
        List2D.set_entry(Std.int(3), Std.int(2), 51, _VarList);
        List2D.set_entry(Std.int(3), Std.int(3), 52, _VarList);
        List2D.set_entry(Std.int(3), Std.int(4), 53, _VarList);



        /* "DEBUG BELOW ONLY" */

    
/* ======================== When Creating ========================= */
        _imageList = new Array<Dynamic>();
propertyChanged("_imageList", _imageList);
        _imageInstList = new Array<Dynamic>();
propertyChanged("_imageInstList", _imageInstList);
        _boolList = new Array<Dynamic>();
propertyChanged("_boolList", _boolList);
        _tempList = new Array<Dynamic>();
propertyChanged("_tempList", _tempList);
        _topImageInstList = new Array<Dynamic>();
propertyChanged("_topImageInstList", _topImageInstList);
        _cooldownImage = new Array<Dynamic>();
propertyChanged("_cooldownImage", _cooldownImage);
        _cooldownImageInst = new Array<Dynamic>();
propertyChanged("_cooldownImageInst", _cooldownImageInst);
        _ListBool2 = new Array<Dynamic>();
propertyChanged("_ListBool2", _ListBool2);
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{
            _boolList.push(true);
            _tempList.push(-1);
            _ListBool2.push(true);
}

    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((hasValue(_actorToRead) != false))
{
            /* "Always update slot" */
            actor.say("drawSkillbar", "_customBlock_setSlotAbi", [0,List2D.get_entry(Std.int(15), Std.int(_actorToRead.getValue("entityDefaultInitilization", "_CharacterID")), getGameAttribute("entityList"))]);
            actor.say("drawSkillbar", "_customBlock_setSlotAbi", [1,List2D.get_entry(Std.int(16), Std.int(_actorToRead.getValue("entityDefaultInitilization", "_CharacterID")), getGameAttribute("entityList"))]);
            actor.say("drawSkillbar", "_customBlock_setSlotAbi", [2,List2D.get_entry(Std.int(17), Std.int(_actorToRead.getValue("entityDefaultInitilization", "_CharacterID")), getGameAttribute("entityList"))]);
            actor.say("drawSkillbar", "_customBlock_setSlotAbi", [3,List2D.get_entry(Std.int(18), Std.int(_actorToRead.getValue("entityDefaultInitilization", "_CharacterID")), getGameAttribute("entityList"))]);
            actor.say("drawSkillbar", "_customBlock_setSlotAbi", [4,List2D.get_entry(Std.int(19), Std.int(_actorToRead.getValue("entityDefaultInitilization", "_CharacterID")), getGameAttribute("entityList"))]);
}

        /* "Draw back" */
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{
            g.alpha = (100/100);
            actor.setAnimation("" + ("" + "skillslotBG"));
            g.moveTo((_gapAftereachSkill * index0), 0);
            actor.drawImage(g);
}

        /* "Draw Skillbars" */
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{
            _tempBool = _boolList[Std.int(index0)];
propertyChanged("_tempBool", _tempBool);
            _tempNum = asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _VarList));
propertyChanged("_tempNum", _tempNum);
            _numInSlot = asNumber(_tempList[Std.int(index0)]);
propertyChanged("_numInSlot", _numInSlot);
            if((!(_tempNum == _numInSlot) && (_tempBool == true)))
{


                if((!(_tempNum == _numInSlot) && !(_tempNum == -1)))
{

                    _tempList[Std.int(index0)] = _tempNum;
                    _imageList[Std.int(index0)] = getExternalImage(List2D.get_entry(Std.int(10), Std.int(_tempNum), getGameAttribute("skillList")));
                    _imageInstList[Std.int(index0)] = new Bitmap(_imageList[Std.int(index0)]);
                    bringImageToBack(_imageInstList[Std.int(index0)]);
                    attachImageToActor(_imageInstList[Std.int(index0)], actor, Std.int((_gapAftereachSkill * index0)), Std.int(0), 1);
                    _boolList[Std.int(index0)] = false;
}

}

            else if((!(_tempNum == _numInSlot) && (_tempBool == false)))
{

                removeImage(_imageInstList[Std.int(index0)]);
                clearImagePartially(_imageList[Std.int(index0)], Std.int((index0 * _gapAftereachSkill)), Std.int(0), Std.int(_gapAftereachSkill), Std.int((actor.getHeight())));
                _boolList[Std.int(index0)] = true;
}

            /* "Tooltip" */
            if((cast((sayToScene("sceneInit", "_customBlock_mouseInArea", [(actor.getX() + (index0 * _gapAftereachSkill)),actor.getY(),((actor.getX() + (index0 * _gapAftereachSkill)) + 64),(actor.getY() + 64)])), Bool) && !(_tempNum == -1)))
{
                g.translateToScreen();
                g.alpha = (75/100);
                _tempxx = asNumber(5);
propertyChanged("_tempxx", _tempxx);
                _tempyy = asNumber(350);
propertyChanged("_tempyy", _tempyy);

                g.setFont(getFont(136));
                g.drawString("" + ("" + List2D.get_entry(Std.int(1), Std.int(_tempNum), getGameAttribute("skillList"))), (_tempxx + 5), (_tempyy + 5));
                g.setFont(getFont(137));
                g.drawString("" + ("" + List2D.get_entry(Std.int(2), Std.int(_tempNum), getGameAttribute("skillList"))), (_tempxx + 10), (_tempyy + 18));

}

}

        if(!(_topDrawn))
{
            for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{

                _TopImage = getExternalImage("SkillBarTop.png");
propertyChanged("_TopImage", _TopImage);
                _TopImageInst = new Bitmap(_TopImage);
propertyChanged("_TopImageInst", _TopImageInst);
                _topImageInstList[Std.int(index0)] = _TopImageInst;
                attachImageToActor(_topImageInstList[Std.int(index0)], actor, Std.int((_gapAftereachSkill * index0)), Std.int(0), 1);
                _topDrawn = true;
propertyChanged("_topDrawn", _topDrawn);
}

}

}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{
            _tempNum3 = asNumber(List2D.get_entry(Std.int(4), Std.int(index0), _VarList));
propertyChanged("_tempNum3", _tempNum3);
            if((_tempNum3 > 0))
{
                List2D.set_entry(Std.int(4), Std.int(index0), (_tempNum3 - (1 / 60)), _VarList);
                g.fillColor = Utils.getColorRGB(255,200,0);
                g.alpha = ((100 * (asNumber(_tempNum3) / asNumber(List2D.get_entry(Std.int(5), Std.int(index0), _VarList))))/100);
                g.fillRect(((-4 - (_gapAftereachSkill * 4)) + (_gapAftereachSkill * index0)), (-4 + 72), 72, -((72 * (asNumber(_tempNum3) / asNumber(List2D.get_entry(Std.int(5), Std.int(index0), _VarList))))));
}

}

}
});
    
/* ======================= Every N seconds ======================== */
runPeriodically(1000 * 1, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        for(index0 in 0...Std.int(getGameAttribute("noSkillslots")))
{
            _tempNum3 = asNumber(List2D.get_entry(Std.int(4), Std.int(index0), _VarList));
propertyChanged("_tempNum3", _tempNum3);
            if((_tempNum3 > 0))
{


}

}

}
}, actor);
    
/* =========================== Any Key ============================ */
addAnyKeyReleasedListener(function(event:KeyboardEvent, list:Array<Dynamic>):Void {
if(wrapper.enabled){

        for(index0 in 0...Std.int(List2D.check_dimensions("rows", _VarList)))
{
            if((event.keyCode == asNumber(List2D.get_entry(Std.int(3), Std.int(index0), _VarList))))
{

                /* "Check if it is unlocked" */
                if((!(asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _VarList)) == -1) && (asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _VarList)) == 1)))
{
                    /* "Check for SKILL ID" */
                    if((asNumber(List2D.get_entry(Std.int(4), Std.int(index0), _VarList)) <= 0))
{
                        /* "Check for mana" */
                        if(((_actorToRead.getValue("entityDefaultInitilization", "_mana") >= List2D.get_entry(Std.int(7), Std.int(asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _VarList))), getGameAttribute("skillList"))) && _actorToRead.getValue("entityDefaultInitilization", "_finishCasting")))
{
                            /* "Get skill info" */
                            sayToScene("sceneInit", "_customBlock_skillCast", [_actorToRead,asNumber(List2D.get_entry(Std.int(2), Std.int(index0), _VarList))]);
                            actor.say("drawSkillbar", "_customBlock_autoSetSlotCd", [index0]);
}

}

}

}

}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
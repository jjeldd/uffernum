package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_9_9_entityDefaultInitilization extends ActorScript
{          	
	
public var _manaMax:Float;

public var _staminaCur:Float;

public var _staminaMax:Float;

public var _baseStr:Float;

public var _bonusStr:Float;

public var _STR:Float;

public var _baseAgi:Float;

public var _bonusAgi:Float;

public var _AGI:Float;

public var _baseInt:Float;

public var _bonusInt:Float;

public var _INT:Float;

public var _solidVit:Float;

public var _baseVit:Float;

public var _baseDef:Float;

public var _VIT:Float;

public var _DEF:Float;

public var _bonusDef:Float;

public var _difScalling:Float;

public var _level:Float;

public var _playerr:Float;

public var _effectsList:Array<Dynamic>;

public var _damageMultiplier:Float;

public var _tempEffNum2:Float;

public var _tempeffNum:Float;

public var _effTrigbegin:String;

public var _statuseffectActor:Actor;

public var _tempCheck:Float;

public var _effTrigsec:String;

public var _effTrigduring:String;

public var _effTrigend:String;

public var _MvSpd:Float;

public var _canCast:Bool;

public var _baseMvSpeed:Float;

public var _percMvSpeed:Float;

public var _percJumpForce:Float;

public var _JmpForce:Float;

public var _canMove:Bool;

public var _baseJumpForce:Float;

public var _healthBase:Float;

public var _healthBonus:Float;

public var _tempSlot:Float;

public var _tempSec:Float;

public var _skillbarActor:Actor;

public var _healthMax:Float;

public var _healthActor:Actor;

public var _health:Float;

public var _attackSpeed:Float;

public var _attackTime:Float;

public var _bonusAttackSpeed:Float;

public var _baseAttackTime:Float;

public var _baseGoldFind:Float;

public var _LUCK:Float;

public var _bonusLuck:Float;

public var _baseLuck:Float;

public var _maxExp:Float;

public var _curExp:Float;

public var _GOLDFIND:Float;

public var _bonusGoldFind:Float;

public var _agiPerLevel:Float;

public var _intPerLevel:Float;

public var _vitPerLevel:Float;

public var _varPointsPerLevel:Float;

public var _levExp:Float;

public var _templevExp:Float;

public var _ccExp:Float;

public var _strPerLevel:Float;

public var _castingTime:Float;

public var _casting:Bool;

public var _castingBarAlpha:Float;

public var _finishCasting:Bool;

public var _invulnerable:Bool;

public var _invulSeconds:Float;

public var _invulCount:Float;

public var _castingSpell:Float;

public var _CharacterID:Float;

public var _cast:Bool;

public var _countAttackTime:Float;

public var _isDead:Bool;

public var _castbarActor:Actor;

public var _castingTimeMax:Float;

public var _rightChargeMax:Float;

public var _rightCharge:Float;

public var _baseManaRegen:Float;

public var _manaActor:Actor;

public var _manaRegen:Float;

public var _bonusManaRegen:Float;

public var _manaBase:Float;

public var _CanBeKnockback:Bool;

public var _mana:Float;

public var _manaBonus:Float;

public var _firstRun:Bool;

public var _canRun:Bool;
    
/* ========================= Custom Block ========================= */


/* Params are: __Damage */
public function _customBlock_dealDamageCal(__Damage:Float):Void
{
var __Self:Actor = actor;
        sayToScene("sceneInit", "_customBlock_actorTakeDamage", [actor,__Damage]);
}
    
/* ========================= Custom Block ========================= */


/* Params are: __damage */
public function _customBlock_taketruedamage(__damage:Float):Void
{
var __Self:Actor = actor;
        sayToScene("sceneInit", "_customBlock_takeTrueDamageGlobal", [actor,__damage]);
}
    
/* ========================= Custom Block ========================= */


/* Params are: __amt */
public function _customBlock_healHealth(__amt:Float):Void
{
var __Self:Actor = actor;
        /* "healing himself" */
        sayToScene("sceneInit", "_customBlock_actorHeal", [actor,__amt]);
}
    
/* ========================= Custom Block ========================= */


/* Params are: __slot __scr */
public function _customBlock_remEff2(__slot:Float, __scr:Bool):Void
{
var __Self:Actor = actor;
        if(__scr)
{
            _tempCheck = asNumber(List2D.get_entry(Std.int(0), Std.int(__slot), _effectsList));
propertyChanged("_tempCheck", _tempCheck);
            if(!(_tempCheck == -1))
{
                /* "Execute end script" */
                _effTrigend = ("" + List2D.get_entry(Std.int(5), Std.int(_tempCheck), getGameAttribute("maineffectsList")));
propertyChanged("_effTrigend", _effTrigend);
                if(!(_effTrigend == ("" + "none")))
{

                    actor.shout("_customEvent_" + _effTrigend);
}

}

}

        List2D.set_entry(Std.int(0), Std.int(__slot), -1, _effectsList);
        List2D.set_entry(Std.int(1), Std.int(__slot), 0, _effectsList);
        List2D.set_entry(Std.int(2), Std.int(__slot), 0, _effectsList);
}
    
/* ========================= Custom Event ========================= */
public function _customEvent_updStats():Void
{
        /* "This section updates our stats when triggered" */
        _DEF = asNumber((_baseDef + _bonusDef));
propertyChanged("_DEF", _DEF);
        _STR = asNumber((_baseStr + _bonusStr));
propertyChanged("_STR", _STR);
        _AGI = asNumber((_baseAgi + _bonusAgi));
propertyChanged("_AGI", _AGI);
        _INT = asNumber((_baseInt + _bonusInt));
propertyChanged("_INT", _INT);
        _VIT = asNumber((_baseVit + _solidVit));
propertyChanged("_VIT", _VIT);
        _LUCK = asNumber((_baseLuck + _bonusLuck));
propertyChanged("_LUCK", _LUCK);
        _GOLDFIND = asNumber(((_baseGoldFind + _bonusGoldFind) + (_LUCK * 0.01)));
propertyChanged("_GOLDFIND", _GOLDFIND);
        _healthMax = asNumber(((_healthBase + _healthBonus) + (_VIT * 10)));
propertyChanged("_healthMax", _healthMax);
        _manaMax = asNumber(((_manaBase + _manaBonus) + (_INT * 5)));
propertyChanged("_manaMax", _manaMax);
        if((_firstRun && _canRun))
{
            _mana = asNumber(_manaMax);
propertyChanged("_mana", _mana);
            _health = asNumber(_healthMax);
propertyChanged("_health", _health);
            _firstRun = false;
propertyChanged("_firstRun", _firstRun);
            trace("" + _manaMax);
            trace("" + _healthMax);
}

        _manaRegen = asNumber(((_baseManaRegen + _bonusManaRegen) + (_INT * 0.1)));
propertyChanged("_manaRegen", _manaRegen);
        _damageMultiplier = asNumber((200 / (200 + _DEF)));
propertyChanged("_damageMultiplier", _damageMultiplier);
        _MvSpd = asNumber((_baseMvSpeed * (1 + _percMvSpeed)));
propertyChanged("_MvSpd", _MvSpd);
        _JmpForce = asNumber((_baseJumpForce * (1 + _percJumpForce)));
propertyChanged("_JmpForce", _JmpForce);
        _attackTime = asNumber((_baseAttackTime / (1 + _bonusAttackSpeed)));
propertyChanged("_attackTime", _attackTime);
        _attackSpeed = asNumber(((1 + _bonusAttackSpeed) / _baseAttackTime));
propertyChanged("_attackSpeed", _attackSpeed);
        _levExp = asNumber((64 + Math.pow(_level, 3)));
propertyChanged("_levExp", _levExp);
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            /* "If it is the player entity" */
            actor.setValue("Jump and Run Movement Edited", "_MaximumRunningSpeed", _MvSpd);
            actor.setValue("Jump and Run Movement Edited", "_JumpingForce", _JmpForce);
}

        /* "Exp to level=64+(CurrentLevel)^3" */
}


 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("manaMax", "_manaMax");
_manaMax = 0.0;
nameMap.set("staminaCur", "_staminaCur");
_staminaCur = 0.0;
nameMap.set("staminaMax", "_staminaMax");
_staminaMax = 0.0;
nameMap.set("baseStr", "_baseStr");
_baseStr = 0.0;
nameMap.set("bonusStr", "_bonusStr");
_bonusStr = 0.0;
nameMap.set("STR", "_STR");
_STR = 0.0;
nameMap.set("baseAgi", "_baseAgi");
_baseAgi = 0.0;
nameMap.set("bonusAgi", "_bonusAgi");
_bonusAgi = 0.0;
nameMap.set("AGI", "_AGI");
_AGI = 0.0;
nameMap.set("baseInt", "_baseInt");
_baseInt = 0.0;
nameMap.set("bonusInt", "_bonusInt");
_bonusInt = 0.0;
nameMap.set("Actor", "actor");
nameMap.set("INT", "_INT");
_INT = 0.0;
nameMap.set("solidVit", "_solidVit");
_solidVit = 0.0;
nameMap.set("baseVit", "_baseVit");
_baseVit = 0.0;
nameMap.set("baseDef", "_baseDef");
_baseDef = 0.0;
nameMap.set("VIT", "_VIT");
_VIT = 0.0;
nameMap.set("DEF", "_DEF");
_DEF = 0.0;
nameMap.set("bonusDef", "_bonusDef");
_bonusDef = 0.0;
nameMap.set("difScalling", "_difScalling");
_difScalling = 0.0;
nameMap.set("level", "_level");
_level = 0.0;
nameMap.set("player", "_playerr");
_playerr = 0.0;
nameMap.set("effectsList", "_effectsList");
_effectsList = [];
nameMap.set("damageMultiplier", "_damageMultiplier");
_damageMultiplier = 0.0;
nameMap.set("tempEffNum2", "_tempEffNum2");
_tempEffNum2 = 0.0;
nameMap.set("tempeffNum", "_tempeffNum");
_tempeffNum = 0.0;
nameMap.set("effTrigbegin", "_effTrigbegin");
_effTrigbegin = "";
nameMap.set("statuseffectActor", "_statuseffectActor");
nameMap.set("tempCheck", "_tempCheck");
_tempCheck = 0.0;
nameMap.set("effTrigsec", "_effTrigsec");
_effTrigsec = "";
nameMap.set("effTrigduring", "_effTrigduring");
_effTrigduring = "";
nameMap.set("effTrigend", "_effTrigend");
_effTrigend = "";
nameMap.set("MvSpd", "_MvSpd");
_MvSpd = 0.0;
nameMap.set("canCast", "_canCast");
_canCast = true;
nameMap.set("baseMvSpeed", "_baseMvSpeed");
_baseMvSpeed = 17.5;
nameMap.set("percMvSpeed", "_percMvSpeed");
_percMvSpeed = 0.0;
nameMap.set("percJumpForce", "_percJumpForce");
_percJumpForce = 0.0;
nameMap.set("JmpForce", "_JmpForce");
_JmpForce = 0.0;
nameMap.set("canMove", "_canMove");
_canMove = false;
nameMap.set("baseJumpForce", "_baseJumpForce");
_baseJumpForce = 195.0;
nameMap.set("healthBase", "_healthBase");
_healthBase = 100.0;
nameMap.set("healthBonus", "_healthBonus");
_healthBonus = 0.0;
nameMap.set("tempSlot", "_tempSlot");
_tempSlot = 0.0;
nameMap.set("tempSec", "_tempSec");
_tempSec = 0.0;
nameMap.set("skillbarActor", "_skillbarActor");
nameMap.set("healthMax", "_healthMax");
_healthMax = 0.0;
nameMap.set("healthActor", "_healthActor");
nameMap.set("health", "_health");
_health = 0.0;
nameMap.set("attackSpeed", "_attackSpeed");
_attackSpeed = 0.0;
nameMap.set("attackTime", "_attackTime");
_attackTime = 0.0;
nameMap.set("bonusAttackSpeed", "_bonusAttackSpeed");
_bonusAttackSpeed = 0.0;
nameMap.set("baseAttackTime", "_baseAttackTime");
_baseAttackTime = 1.3;
nameMap.set("baseGoldFind", "_baseGoldFind");
_baseGoldFind = 0.0;
nameMap.set("LUCK", "_LUCK");
_LUCK = 0.0;
nameMap.set("bonusLuck", "_bonusLuck");
_bonusLuck = 0.0;
nameMap.set("baseLuck", "_baseLuck");
_baseLuck = 0.0;
nameMap.set("maxExp", "_maxExp");
_maxExp = 0.0;
nameMap.set("curExp", "_curExp");
_curExp = 0.0;
nameMap.set("GOLDFIND", "_GOLDFIND");
_GOLDFIND = 0.0;
nameMap.set("bonusGoldFind", "_bonusGoldFind");
_bonusGoldFind = 0.0;
nameMap.set("agiPerLevel", "_agiPerLevel");
_agiPerLevel = 0.0;
nameMap.set("intPerLevel", "_intPerLevel");
_intPerLevel = 0.0;
nameMap.set("vitPerLevel", "_vitPerLevel");
_vitPerLevel = 0.0;
nameMap.set("varPointsPerLevel", "_varPointsPerLevel");
_varPointsPerLevel = 0.0;
nameMap.set("levExp", "_levExp");
_levExp = 0.0;
nameMap.set("templevExp", "_templevExp");
_templevExp = 0.0;
nameMap.set("ccExp", "_ccExp");
_ccExp = 0.0;
nameMap.set("strPerLevel", "_strPerLevel");
_strPerLevel = 0.0;
nameMap.set("castingTime", "_castingTime");
_castingTime = 0.0;
nameMap.set("casting", "_casting");
_casting = false;
nameMap.set("castingBarAlpha", "_castingBarAlpha");
_castingBarAlpha = 0.0;
nameMap.set("finishCasting", "_finishCasting");
_finishCasting = true;
nameMap.set("invulnerable", "_invulnerable");
_invulnerable = false;
nameMap.set("invulSeconds", "_invulSeconds");
_invulSeconds = 0.5;
nameMap.set("invulCount", "_invulCount");
_invulCount = 0.0;
nameMap.set("castingSpell", "_castingSpell");
_castingSpell = -1.0;
nameMap.set("Character ID", "_CharacterID");
_CharacterID = -1.0;
nameMap.set("cast", "_cast");
_cast = false;
nameMap.set("countAttackTime", "_countAttackTime");
_countAttackTime = 0.0;
nameMap.set("isDead", "_isDead");
_isDead = false;
nameMap.set("castbarActor", "_castbarActor");
nameMap.set("castingTimeMax", "_castingTimeMax");
_castingTimeMax = 0.0;
nameMap.set("rightChargeMax", "_rightChargeMax");
_rightChargeMax = 100.0;
nameMap.set("rightCharge", "_rightCharge");
_rightCharge = 0.0;
nameMap.set("baseManaRegen", "_baseManaRegen");
_baseManaRegen = 1.2;
nameMap.set("manaActor", "_manaActor");
nameMap.set("manaRegen", "_manaRegen");
_manaRegen = 0.0;
nameMap.set("bonusManaRegen", "_bonusManaRegen");
_bonusManaRegen = 0.0;
nameMap.set("manaBase", "_manaBase");
_manaBase = 0.0;
nameMap.set("Can Be Knockback", "_CanBeKnockback");
_CanBeKnockback = true;
nameMap.set("mana", "_mana");
_mana = 0.0;
nameMap.set("manaBonus", "_manaBonus");
_manaBonus = 0.0;
nameMap.set("firstRun", "_firstRun");
_firstRun = true;
nameMap.set("canRun", "_canRun");
_canRun = false;

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        _canCast = true;
propertyChanged("_canCast", _canCast);
        _canMove = true;
propertyChanged("_canMove", _canMove);
        /* "Below checks if it is a player. Enables/disables movement depending on the result" */
        if(!(_playerr == asNumber(1)))
{
            actor.disableBehavior("Jump and Run Movement Edited");
}

        else
{
            actor.enableBehavior("Jump and Run Movement Edited");

}

    
/* ======================== When Creating ========================= */
        /* "Pulls initial base stats from the list grid stored in Character ID game attribute" */
        runLater(1000 * 0.5, function(timeTask:TimedTask):Void {
                    if(!(_CharacterID == -1))
{
                        /* "If character ID exists" */
                        _healthBase = asNumber(List2D.get_entry(Std.int(3), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_healthBase", _healthBase);
                        _manaBase = asNumber(List2D.get_entry(Std.int(4), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_manaBase", _manaBase);
                        _baseStr = asNumber(List2D.get_entry(Std.int(5), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseStr", _baseStr);
                        _baseAgi = asNumber(List2D.get_entry(Std.int(6), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseAgi", _baseAgi);
                        _baseInt = asNumber(List2D.get_entry(Std.int(7), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseInt", _baseInt);
                        _baseVit = asNumber(List2D.get_entry(Std.int(8), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseVit", _baseVit);
                        _baseLuck = asNumber(List2D.get_entry(Std.int(9), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseLuck", _baseLuck);
                        _baseDef = asNumber(List2D.get_entry(Std.int(10), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseDef", _baseDef);
                        _baseMvSpeed = asNumber(List2D.get_entry(Std.int(11), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseMvSpeed", _baseMvSpeed);
                        _baseJumpForce = asNumber(List2D.get_entry(Std.int(12), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseJumpForce", _baseJumpForce);
                        _baseAttackTime = asNumber(List2D.get_entry(Std.int(13), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseAttackTime", _baseAttackTime);
                        _baseGoldFind = asNumber(List2D.get_entry(Std.int(14), Std.int(_CharacterID), getGameAttribute("entityList")));
propertyChanged("_baseGoldFind", _baseGoldFind);
                        _canRun = true;
propertyChanged("_canRun", _canRun);
}

}, actor);
    
/* ======================== When Creating ========================= */
        /* "After checking if it is a player it will create the health actor." */
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            createRecycledActor(getActorType(20), 0, 0, Script.MIDDLE);
            _healthActor = getLastCreatedActor();
propertyChanged("_healthActor", _healthActor);

            _healthActor.setValue("drawHealthbar2", "_actorToRead", actor);
}

    
/* ======================== When Creating ========================= */
        /* "Same as healthINI but mana" */
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            createRecycledActor(getActorType(177), 0, 0, Script.MIDDLE);
            _manaActor = getLastCreatedActor();
propertyChanged("_manaActor", _manaActor);

            _manaActor.setValue("drawManabar", "_actorToRead", actor);
}

    
/* ======================== When Creating ========================= */
        /* "\"\"\"\"" */
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            createRecycledActor(getActorType(0), 0, 0, Script.MIDDLE);
            _skillbarActor = getLastCreatedActor();
propertyChanged("_skillbarActor", _skillbarActor);
            _skillbarActor.setValue("drawSkillbar", "_actorToRead", actor);
            createRecycledActor(getActorType(128), 0, 0, Script.MIDDLE);
            _castbarActor = getLastCreatedActor();
propertyChanged("_castbarActor", _castbarActor);
            _castbarActor.setValue("castbarBehaviour", "_actorToRead", actor);
}

    
/* ======================== When Creating ========================= */
        /* "Status effects made using 2d lists extension" */
        /* "Each column in the list has a variable check .txt for help" */
        _effectsList = List2D.new_2D(Std.int(5), Std.int(10));
propertyChanged("_effectsList", _effectsList);
        /* "Resets each slot on creation!" */
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", _effectsList)))
{
            actor.say("entityDefaultInitilization", "_customBlock_remEff2", [index0,false]);
}

        /* "Below creates the status effect actor" */
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            createRecycledActor(getActorType(52), 0, 0, Script.FRONT);
            _statuseffectActor = getLastCreatedActor();
propertyChanged("_statuseffectActor", _statuseffectActor);
            _statuseffectActor.setValue("statusEffects", "_actorToRead", actor);
}

    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        actor.say("entityDefaultInitilization", "_customEvent_" + "updStats");
        _mana = asNumber((_mana + (_manaRegen / 60)));
propertyChanged("_mana", _mana);
}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((_castingBarAlpha > 0))
{

            if((_castingTime < _castingTimeMax))
{
                /* "While Casting." */
                _castingTime = asNumber((_castingTime + (1 / 60)));
propertyChanged("_castingTime", _castingTime);
                if(!(_castingSpell == -1))
{
                    if((!(List2D.get_entry(Std.int(4), Std.int(_castingSpell), getGameAttribute("skillList")) == "None") && !(asNumber(List2D.get_entry(Std.int(4), Std.int(_castingSpell), getGameAttribute("skillList"))) == -1)))
{
                        /* "Script During" */
                        actor.shout("_customEvent_" + List2D.get_entry(Std.int(4), Std.int(_castingSpell), getGameAttribute("skillList")));
}

}

}

            else if(((_castingTime >= _castingTimeMax) && !(_finishCasting)))
{
                /* "Finish Casting" */
                if(!(_castingSpell == -1))
{
                    if((!(List2D.get_entry(Std.int(5), Std.int(_castingSpell), getGameAttribute("skillList")) == "None") && !(asNumber(List2D.get_entry(Std.int(5), Std.int(_castingSpell), getGameAttribute("skillList"))) == -1)))
{
                        /* "Finish Script" */
                        actor.shout("_customEvent_" + List2D.get_entry(Std.int(5), Std.int(_castingSpell), getGameAttribute("skillList")));
                        sayToScene("sceneInit", "_customBlock_actorTakeMana", [actor,asNumber(List2D.get_entry(Std.int(7), Std.int(_castingSpell), getGameAttribute("skillList")))]);

}

                    _finishCasting = true;
propertyChanged("_finishCasting", _finishCasting);
}

}

            if((_finishCasting && (_castingBarAlpha > 0)))
{
                _castingBarAlpha = asNumber(0);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
}

            if((asNumber(_castingBarAlpha) <= 0))
{
                /* "Fail Safe 2" */
                _castingBarAlpha = asNumber(0);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                _castingTime = asNumber(0);
propertyChanged("_castingTime", _castingTime);
                _castingTimeMax = asNumber(0);
propertyChanged("_castingTimeMax", _castingTimeMax);
                _finishCasting = true;
propertyChanged("_finishCasting", _finishCasting);
                _casting = false;
propertyChanged("_casting", _casting);
}

}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((_invulCount > 0))
{
            _invulnerable = true;
propertyChanged("_invulnerable", _invulnerable);
}

        else if((_invulCount <= 0))
{
            _invulnerable = false;
propertyChanged("_invulnerable", _invulnerable);
}

}
});
    
/* ======================= Every N seconds ======================== */
runPeriodically(1000 * 0.5, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        if((_invulCount > 0))
{
            _invulCount = asNumber((_invulCount - 0.5));
propertyChanged("_invulCount", _invulCount);
}

}
}, actor);
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            if(!(_ccExp == _curExp))
{
                if((_ccExp > _curExp))
{
                    _ccExp = asNumber((_ccExp - ((_ccExp - _curExp) * 0.1)));
propertyChanged("_ccExp", _ccExp);
}

                else
{
                    _ccExp = asNumber((_ccExp + ((_curExp - _ccExp) * 0.1)));
propertyChanged("_ccExp", _ccExp);
}

}

            if((_curExp >= _levExp))
{
                /* "Level UP!" */
                _templevExp = asNumber((_curExp - _levExp));
propertyChanged("_templevExp", _templevExp);
                _level += 1;
propertyChanged("_level", _level);
                _bonusStr = asNumber((_bonusStr + _strPerLevel));
propertyChanged("_bonusStr", _bonusStr);
                _bonusAgi = asNumber((_bonusAgi + _agiPerLevel));
propertyChanged("_bonusAgi", _bonusAgi);
                _bonusInt = asNumber((_bonusInt + _intPerLevel));
propertyChanged("_bonusInt", _bonusInt);
                _solidVit = asNumber((_solidVit + _vitPerLevel));
propertyChanged("_solidVit", _solidVit);
                _curExp = asNumber(_templevExp);
propertyChanged("_curExp", _curExp);
}

}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", _effectsList)))
{
            _tempEffNum2 = asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _effectsList));
propertyChanged("_tempEffNum2", _tempEffNum2);
            _tempCheck = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), _effectsList));
propertyChanged("_tempCheck", _tempCheck);
            if(!(_tempCheck == -1))
{
                if((_tempEffNum2 > 0))
{
                    List2D.set_entry(Std.int(1), Std.int(index0), (_tempEffNum2 - (1 / 60)), _effectsList);
                    /* "Execute during script" */
                    _effTrigduring = ("" + List2D.get_entry(Std.int(4), Std.int(_tempCheck), getGameAttribute("maineffectsList")));
propertyChanged("_effTrigduring", _effTrigduring);
                    if(!(_effTrigduring == ("" + "none")))
{

                        actor.shout("_customEvent_" + _effTrigduring);
}

}

                else
{
                    actor.say("entityDefaultInitilization", "_customBlock_remEff2", [index0,true]);
}

}

}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            if((_countAttackTime <= 0))
{
                if(isMouseDown())
{
                    /* "Left" */
                    if(((_castingSpell == -1) && !(("" + List2D.get_entry(Std.int(20), Std.int(_CharacterID), getGameAttribute("entityList"))) == "None")))
{
                        /* "Charging Right Attack" */
                        _castingTimeMax = asNumber(_rightChargeMax);
propertyChanged("_castingTimeMax", _castingTimeMax);
                        if((_castingTime < _castingTimeMax))
{
                            _finishCasting = false;
propertyChanged("_finishCasting", _finishCasting);
                            _castingBarAlpha = asNumber(1);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                            _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                            _castingTime = asNumber((_castingTime + 1));
propertyChanged("_castingTime", _castingTime);
}

}

                    if(isKeyDown("Right Attack"))
{
                        /* "Right" */
                        if(((_castingSpell == -1) && !(("" + List2D.get_entry(Std.int(21), Std.int(_CharacterID), getGameAttribute("entityList"))) == "None")))
{
                            /* "Charging Right Attack" */
                            _castingTimeMax = asNumber(_rightChargeMax);
propertyChanged("_castingTimeMax", _castingTimeMax);
                            if((_castingTime < _castingTimeMax))
{
                                _finishCasting = false;
propertyChanged("_finishCasting", _finishCasting);
                                _castingBarAlpha = asNumber(1);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                                _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                                _castingTime = asNumber((_castingTime + 1));
propertyChanged("_castingTime", _castingTime);
}

}

}

                    else
{

}

}

                if(isMouseReleased())
{
                    /* "Left" */
                    if(!(isKeyDown("Right Attack")))
{
                        if(((_castingSpell == -1) && !(("" + List2D.get_entry(Std.int(20), Std.int(_CharacterID), getGameAttribute("entityList"))) == "None")))
{
                            actor.shout("_customEvent_" + List2D.get_entry(Std.int(20), Std.int(_CharacterID), getGameAttribute("entityList")));
                            _finishCasting = true;
propertyChanged("_finishCasting", _finishCasting);
                            _castingBarAlpha = asNumber(0);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                            _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                            _castingTime = asNumber(0);
propertyChanged("_castingTime", _castingTime);
                            _castingTimeMax = asNumber(0);
propertyChanged("_castingTimeMax", _castingTimeMax);
                            _countAttackTime = asNumber(_attackTime);
propertyChanged("_countAttackTime", _countAttackTime);
}

}

                    if(isKeyDown("Right Attack"))
{
                        /* "Right" */
                        if(((_castingSpell == -1) && !(("" + List2D.get_entry(Std.int(21), Std.int(_CharacterID), getGameAttribute("entityList"))) == "None")))
{
                            /* "Right Attack" */
                            actor.shout("_customEvent_" + List2D.get_entry(Std.int(21), Std.int(_CharacterID), getGameAttribute("entityList")));
                            _finishCasting = true;
propertyChanged("_finishCasting", _finishCasting);
                            _castingBarAlpha = asNumber(0);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                            _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                            _castingTime = asNumber(0);
propertyChanged("_castingTime", _castingTime);
                            _castingTimeMax = asNumber(0);
propertyChanged("_castingTimeMax", _castingTimeMax);
                            _countAttackTime = asNumber(_attackTime);
propertyChanged("_countAttackTime", _countAttackTime);
}

}

}

}

            /* "Shift key release/pressed fix" */
            if(((_castingSpell == -1) && (isKeyReleased("Right Attack") || isKeyPressed("Right Attack"))))
{
                _finishCasting = true;
propertyChanged("_finishCasting", _finishCasting);
                _castingBarAlpha = asNumber(0);
propertyChanged("_castingBarAlpha", _castingBarAlpha);
                _castingSpell = asNumber(-1);
propertyChanged("_castingSpell", _castingSpell);
                _castingTime = asNumber(0);
propertyChanged("_castingTime", _castingTime);
                _castingTimeMax = asNumber(0);
propertyChanged("_castingTimeMax", _castingTimeMax);
}

}

        /* "Reduction of countAttackTime" */
        if((_countAttackTime > 0))
{
            _countAttackTime = asNumber((_countAttackTime - (1 / 60)));
propertyChanged("_countAttackTime", _countAttackTime);
}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(_canRun)
{
            if((_health <= 0))
{
                _isDead = true;
propertyChanged("_isDead", _isDead);
}

            else
{
                _isDead = false;
propertyChanged("_isDead", _isDead);
}

}

}
});
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(_isDead)
{
            if(!(_playerr == asNumber(1)))
{
                recycleActor(actor);
}

            else
{
                switchScene(GameModel.get().scenes.get(4).getID(), createFadeOut((1)),createFadeIn((0.5)));
                recycleActor(actor);
}

}

}
});
    
/* ======================= Every N seconds ======================== */
runPeriodically(1000 * 1, function(timeTask:TimedTask):Void {
if(wrapper.enabled){
        for(index0 in 0...Std.int(List2D.check_dimensions("rows", _effectsList)))
{
            _tempEffNum2 = asNumber(List2D.get_entry(Std.int(1), Std.int(index0), _effectsList));
propertyChanged("_tempEffNum2", _tempEffNum2);
            _tempCheck = asNumber(List2D.get_entry(Std.int(0), Std.int(index0), _effectsList));
propertyChanged("_tempCheck", _tempCheck);
            if(!(_tempCheck == -1))
{
                if((_tempEffNum2 > 0))
{
                    /* "Execute every second script" */
                    _effTrigsec = ("" + List2D.get_entry(Std.int(7), Std.int(_tempCheck), getGameAttribute("maineffectsList")));
propertyChanged("_effTrigsec", _effTrigsec);
                    if(!(_effTrigsec == ("" + "none")))
{

                        actor.shout("_customEvent_" + _effTrigsec);
}

}

}

}

}
}, actor);
    
/* =========================== Keyboard =========================== */
addKeyStateListener("Potion", function(pressed:Bool, released:Bool, list:Array<Dynamic>):Void {
if(wrapper.enabled && pressed){
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            if(!(cast((sayToScene("sceneInit", "_customBlock_CheckForEffect", [actor,10])), Float) == -1))
{
                /* "Already using Potion" */
}

            else
{
                if((getGameAttribute("currentPotions") > 0))
{
                    setGameAttribute("currentPotions", (getGameAttribute("currentPotions") - 1));
                    sayToScene("sceneInit", "_customBlock_actorApplyEffect", [actor,10,4]);
}

}

}

}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if(!(_playerr == asNumber(1)))
{

}

        else
{
            g.translateToScreen();
            g.moveTo(0, 0);
            g.alpha = (75/100);
            g.fillColor = Utils.getColorRGB(51,51,51);
            g.fillRect(0, 0, getScreenWidth(), 8);
            g.fillColor = Utils.getColorRGB(255,200,0);
            g.fillRect(0, 0, (getScreenWidth() * (_ccExp / _levExp)), 7);
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}
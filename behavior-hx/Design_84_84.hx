package scripts;

import com.stencyl.graphics.G;

import com.stencyl.behavior.Script;
import com.stencyl.behavior.ActorScript;
import com.stencyl.behavior.SceneScript;
import com.stencyl.behavior.TimedTask;

import com.stencyl.models.Actor;
import com.stencyl.models.GameModel;
import com.stencyl.models.actor.Animation;
import com.stencyl.models.actor.ActorType;
import com.stencyl.models.actor.Collision;
import com.stencyl.models.actor.Group;
import com.stencyl.models.Scene;
import com.stencyl.models.Sound;
import com.stencyl.models.Region;
import com.stencyl.models.Font;

import com.stencyl.Engine;
import com.stencyl.Input;
import com.stencyl.Key;
import com.stencyl.utils.Utils;

import nme.ui.Mouse;
import nme.display.Graphics;
import nme.display.BlendMode;
import nme.display.BitmapData;
import nme.display.Bitmap;
import nme.events.Event;
import nme.events.KeyboardEvent;
import nme.events.TouchEvent;
import nme.net.URLLoader;

import box2D.dynamics.joints.B2Joint;

import motion.Actuate;
import motion.easing.Back;
import motion.easing.Cubic;
import motion.easing.Elastic;
import motion.easing.Expo;
import motion.easing.Linear;
import motion.easing.Quad;
import motion.easing.Quart;
import motion.easing.Quint;
import motion.easing.Sine;

import com.stencyl.graphics.shaders.BasicShader;
import com.stencyl.graphics.shaders.GrayscaleShader;
import com.stencyl.graphics.shaders.SepiaShader;
import com.stencyl.graphics.shaders.InvertShader;
import com.stencyl.graphics.shaders.GrainShader;
import com.stencyl.graphics.shaders.ExternalShader;
import com.stencyl.graphics.shaders.InlineShader;
import com.stencyl.graphics.shaders.BlurShader;
import com.stencyl.graphics.shaders.ScanlineShader;
import com.stencyl.graphics.shaders.CSBShader;
import com.stencyl.graphics.shaders.HueShader;
import com.stencyl.graphics.shaders.TintShader;
import com.stencyl.graphics.shaders.BloomShader;



class Design_84_84 extends ActorScript
{          	
	
public var _hpReal:Float;

public var _hpMax:Float;

public var _hpCur:Float;

public var _actorToRead:Actor;

public var _topDrawn:Bool;

public var _img:BitmapData;

public var _imgInst:Bitmap;

 
 	public function new(dummy:Int, actor:Actor, engine:Engine)
	{
		super(actor, engine);	
		nameMap.set("mpReal", "_hpReal");
_hpReal = 0.0;
nameMap.set("mpMax", "_hpMax");
_hpMax = 0.0;
nameMap.set("mpCur", "_hpCur");
_hpCur = 0.0;
nameMap.set("actorToRead", "_actorToRead");
nameMap.set("topDrawn", "_topDrawn");
_topDrawn = false;
nameMap.set("img", "_img");
nameMap.set("imgInst", "_imgInst");
nameMap.set("Actor", "actor");

	}
	
	override public function init()
	{
		    
/* ======================== When Creating ========================= */
        _hpCur = asNumber(actor.getValue("entityDefaultInitilization", "_manaMax"));
propertyChanged("_hpCur", _hpCur);
        /* "Creates the outer image of the mana bottle" */
        _img = getExternalImage("manaBottle.png");
propertyChanged("_img", _img);
        _imgInst = new Bitmap(_img);
propertyChanged("_imgInst", _imgInst);
        /* "Scale bottle to fit" */
        _imgInst.scaleX = (50/100);
        _imgInst.scaleY = (50/100);
        /* "Attach bottle to screen" */
        attachImageToActor(_imgInst, actor, Std.int(-18), Std.int(-20), 1);
    
/* ======================== When Updating ========================= */
addWhenUpdatedListener(null, function(elapsedTime:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((hasValue(_actorToRead) != false))
{
            /* "Pulls variables from actorToRead" */
            _hpReal = asNumber(_actorToRead.getValue("entityDefaultInitilization", "_mana"));
propertyChanged("_hpReal", _hpReal);
            _hpMax = asNumber(_actorToRead.getValue("entityDefaultInitilization", "_manaMax"));
propertyChanged("_hpMax", _hpMax);


            if(!(Math.round(_hpCur) == Math.round(_hpReal)))
{
                /* "Lerping" */
                _hpCur = asNumber(cast((sayToScene("sceneInit", "_customBlock_lerpVal", [_hpCur,_hpReal,0.1])), Float));
propertyChanged("_hpCur", _hpCur);
}

}

}
});
    
/* ========================= When Drawing ========================= */
addWhenDrawingListener(null, function(g:G, x:Float, y:Float, list:Array<Dynamic>):Void {
if(wrapper.enabled){
        if((hasValue(_actorToRead) != false))
{
            g.alpha = (100/100);
            actor.setAnimation("" + ("" + "mpAnim"));
            actor.setCurrentFrame(Std.int(Math.ceil((actor.getNumFrames() * (1 - ((Math.ceil(_hpCur) + 1) / _hpMax))))));

            actor.drawImage(g);
}

}
});

	}	      	
	
	override public function forwardMessage(msg:String)
	{
		
	}
}